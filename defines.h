#ifndef DEFINES_H
#define DEFINES_H

#pragma once

#include <QString>
#include <QDir>
#include <QtSql>
#include <QStringList>

//sudo apt install libqt5sql5-mysql

// Строковые константы
const QString PROGRAMM_NAME = "Truant";
const QString VERSION = "1.0";
const QString BUILD_ID = "000001";
const QString RELEASE = VERSION + "." + BUILD_ID;
const QString FULL_PROGRAMM_NAME = PROGRAMM_NAME + " v" + VERSION;
const QString LOG_NAME = PROGRAMM_NAME + "_" + VERSION + ".log";
QString static INI_FILE = QDir::currentPath() + "/" + PROGRAMM_NAME.toLower() + ".ini";

// Для spashScreen
//static const int LOAD_TIME_MSEC = 5 * 1000;
static const int PROGRESS_X_PX = 27;
static const int PROGRESS_Y_PX = 153;
static const int PROGRESS_WIDTH_PX = 323;
static const int PROGRESS_HEIGHT_PX = 23;

enum typeBase {
    MSQL,
    PSQL
};

struct School {
    School():
        all(0),
        ill(0),
        other(0),
        currentID(0),
        wDate(QDate::currentDate())
    {

    }
    int all;         // Всего по списку
    int ill;         // Отсутствуют по болезни
    int other;       // ..по другим причинам
    int currentID;   // текущий класс
    QDate wDate;     // рабочая дата
    void Clear()
    {
        all = 0;
        ill = 0;
        other = 0;
        currentID = 0;
        wDate = QDate::currentDate();
    }
};

struct Grade {
    Grade():
        id(0),
        dig(0),
        let(""),
        pupil(0),
        absent(0),
        ill(0),
        other(0),
        comment(""),
        done(false)
    {

    }
    int id;           // id      - уникальный идентификатор в БД
    int dig;          // dig     - цифра класса
    QString let;      // let     - буква класса
    int pupil;        // pupil   - кол-во по списку
    int absent;       // absent  - всего отсутствуют absent = sick + other
    int ill;         // sick    - отсутствуют по болезни
    int other;        // other   - отсутствуют по другим причинам
    QString comment;  // comment - примечание, на первом этапе храним там ФИО прогульщиков
    bool done;        // done    - данные по остутствующим заполнены на дату
    void Clear() {
        id = 0;
        dig = 0;
        let = "";
        pupil = 0;
        absent = 0;
        ill = 0;
        other = 0;
        comment = "";
        done = false;
    }
};

struct Codes {
    QStringList winCodes;
    QStringList utfCodes;
};

#endif // DEFINES_H
