#include "options.h"
#include "../settings.h"
#include "ui_options.h"

extern bool isAdmin;
extern Settings settings;

Options::Options(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Options)
{
    ui->setupUi(this);
}

Options::~Options()
{
    delete ui;
}

void Options::showEvent(QShowEvent *ev)
{
    QDialog::showEvent(ev);
    showEventHelper();
}

// отрисовка формы
void Options::showEventHelper()
{
    ui->deDateBegin->setDate(settings.dateStart);
    ui->deDateEnd->setDate(settings.dateEnd);
    if (isAdmin){
        ui->cbPosition->setChecked(settings.getPosWindow());
        ui->cbSize->setChecked(settings.getSizeWindow());
        ui->cbQuit->setChecked(settings.getQuitNoAsk());
        if (settings.getShowFullGrid()) {
            ui->rbFull->setChecked(true);
            ui->rbParallel->setChecked(false);
        } else {
            ui->rbFull->setChecked(false);
            ui->rbParallel->setChecked(true);
        }
        ui->lePathOut->setText(settings.getPathOut());
        ui->sbTimer->setValue(settings.getTimeRefresh());
    }else{
        ui->cbPosition->setChecked(settings.getPosWindow());
        ui->cbQuit->setChecked(settings.getQuitNoAsk());
        ui->cbSize->setDisabled(true);
        ui->rbFull->setDisabled(true);
        ui->rbParallel->setDisabled(true);
        ui->label_2->setDisabled(true);
        ui->label_3->setDisabled(true);
        ui->label_4->setDisabled(true);
        ui->label_5->setDisabled(true);
        ui->lePathOut->setDisabled(true);
        ui->groupBox->setDisabled(true);
        ui->groupBox_2->setDisabled(true);
        ui->leFile2Base->setDisabled(true);
        ui->tbFile2Base->setDisabled(true);
        ui->tbPathOut->setDisabled(true);
        ui->lbTimer->setDisabled(true);
        ui->sbTimer->setDisabled(true);
    }
}

void Options::on_buttonBox_accepted()
{
    if (isAdmin){
        if (ui->cbQuit->isChecked()) {
            settings.setQuitNoAsk(true);
        } else {
            settings.setQuitNoAsk(false);
        }
        if (ui->cbPosition->isChecked()) {
            settings.setPosWindow(true);
        } else {
            settings.setPosWindow(false);
        }
        if (ui->cbSize->isChecked()) {
            settings.setSizeWindow(true);
        } else {
            settings.setSizeWindow(false);
        }
        settings.setTimeRefresh(ui->sbTimer->value());
        if (ui->rbFull->isChecked()) {
            settings.setShowFullGrid(true);
        } else if (ui->rbParallel->isChecked()) {
            settings.setShowFullGrid(false);
        }
        settings.setPathOut(ui->lePathOut->text());        
    }else{
        if (ui->cbQuit->isChecked()) {
            settings.setQuitNoAsk(true);
        } else {
            settings.setQuitNoAsk(false);
        }
        if (ui->cbPosition->isChecked()) {
            settings.setPosWindow(true);
        } else {
            settings.setPosWindow(false);
        }
    }
    if (!settings.writeConfig()) {
        QMessageBox::warning(nullptr, FULL_PROGRAMM_NAME, "Изменения сохранить не удалось!");
    }
}

// выбрать файл для загрузки в базу
void Options::on_tbFile2Base_clicked()
{
    ui->leFile2Base->setText(QFileDialog::getOpenFileName(this,
                                                          QString::fromUtf8("Открыть файл"),
                                                          QDir::currentPath(),
                                                          "Файл для загрузки (*.csv);;All files (*.*)"));
}

void Options::on_pbLoad_clicked()
{
    if (ui->leFile2Base->text().length() == 0) {
        QMessageBox::warning(nullptr, FULL_PROGRAMM_NAME, "Укажите файл для загрузки!");
        return;
    } else {
        QFile file(ui->leFile2Base->text());
        if ( !file.open(QFile::ReadOnly | QFile::Text) ) {
             QMessageBox::warning(nullptr, FULL_PROGRAMM_NAME, "Файл для загрузки не найден!");
             return;
        }
        QProgressDialog progress;
        progress.setLabelText("Загрузка данных...");
        progress.setWindowModality(Qt::WindowModal);
        progress.setMinimum(0);
        progress.setMaximum(0);
        progress.setValue(0);
        progress.show();
        if (settings.loadFile2Base(ui->deDateBegin->date(), ui->deDateEnd->date(), ui->leFile2Base->text())) {
            QMessageBox::information(nullptr, FULL_PROGRAMM_NAME, "Данные загружены успешно!");
        } else {
            QMessageBox::critical(nullptr, FULL_PROGRAMM_NAME, "Данные загрузить не удалось!\nИли они загружены с ошибками!\n\nВозможно повреждение данных!");
        }
    }
}

void Options::on_tbPathOut_clicked()
{
    ui->lePathOut->setText(QFileDialog::getExistingDirectory(this, "Выберите папку для выгрузки отчетов:", ui->lePathOut->text()));
}

void Options::on_buttonBox_clicked(QAbstractButton *button)
{
    if (isAdmin) {
        // TODO: проверять на наличие файлов и путей, ругаться если их нет и не давать сохранить.
        if ( button == ui->buttonBox->button(QDialogButtonBox::Ok) ) {
            if ((ui->lePathOut->text().length() == 0) || (!QDir(ui->lePathOut->text()).exists())) {
                QMessageBox::warning(nullptr, FULL_PROGRAMM_NAME, "Папка для выгрузки отчетов не найдена!");
                disconnect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
            } else {
                connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
            }
        }
    }
}
