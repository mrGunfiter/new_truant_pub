#ifndef EDITTRUANTS_H
#define EDITTRUANTS_H

#include <QDialog>

namespace Ui {
class EditTruants;
}

class EditTruants : public QDialog
{
    Q_OBJECT

public:
    explicit EditTruants(QWidget *parent = nullptr);
    ~EditTruants();
protected:
    void showEvent(QShowEvent *ev);

private slots:
    void on_buttonBox_accepted();

private:
    void showEventHelper();
    Ui::EditTruants *ui;
};

#endif // EDITTRUANTS_H
