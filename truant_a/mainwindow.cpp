#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDir>
#include <thread>

extern int isOK;
extern bool isAdmin;
extern Settings settings;
extern Grade currentGrade;
extern School mySchool;
extern QVector <Grade> allGrades;
extern QVector <Grade> gradesParallel;
extern bool editGrade;
extern QString executerName;

static bool check = false;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle(FULL_PROGRAMM_NAME);
    this->setWindowIcon(QIcon(":/ico/truant_a.ico"));
    ui->lbLic->setVisible(false);
    ui->tbv_all->installEventFilter(this);
    step = 0;
    if (isOK == 2) {
        timer1 = new QTimer(); // обновление окна
        connect(timer1, SIGNAL(timeout()), this, SLOT(slotTimer1()));
        timer1->start(5000);
        timer2 = new QTimer(); // чтение рег.инфо
        connect(timer2, SIGNAL(timeout()), this, SLOT(slotTimer2()));
        timer2->start(3000);
        timer3 = new QTimer(); // чтение рег.инфо
        connect(timer3, SIGNAL(timeout()), this, SLOT(slotTimer3()));
        timer3->start(10000);
    }
}

// таймер обновления данных
void MainWindow::slotTimer1()
{
    settings.getSchool(ui->deDate->date());
    fillTable();
    fillMainTable(0);
}

void MainWindow::slotTimer2()
{
    checkLic();
}

void MainWindow::slotTimer3()
{
    QStringList tmpStrList;
    tmpStrList = checkLicExp();
    ui->lbLic->setVisible(true);
    ui->lbLic->setText(tmpStrList[0] + "\n" + tmpStrList[1]);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::showEvent(QShowEvent *ev)
{
    QMainWindow::showEvent(ev);
    showEventHelper();    
    if (settings.getPosWindow()) {
        move(settings.getXPos(), settings.getYPos());
    }
    if (settings.getSizeWindow()) {
        resize(settings.getWSize(), settings.getHSize());
    }
    check = true;
    checkActivation();
    check = false;
    checkLicExp();
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    event->ignore();
    if (settings.getQuitNoAsk()) {
        savePosSize();
        settings.writeConfig();
        event->accept();
    } else {
        QMessageBox msgBox;
        msgBox.setWindowTitle(FULL_PROGRAMM_NAME);
        msgBox.setText("Завершить работу?");
        msgBox.setIcon(QMessageBox::Question);
        msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
        msgBox.setDefaultButton(QMessageBox::Cancel);
        if  (msgBox.exec() == QMessageBox::Ok) {
            savePosSize();
            settings.writeConfig();
            event->accept();
        }
    }
}

// Ловим нажатие стрелок в таблице
bool MainWindow::eventFilter(QObject *target, QEvent *event)
{
    int row = 0;
    if (target == ui->tbv_all) {
        if (event->type() == QEvent::KeyPress) {
            QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
            if ((keyEvent->key() == Qt::Key_Up) || (keyEvent->key() == Qt::Key_Down)) {
                if (keyEvent->key() == Qt::Key_Up) {
                    if (ui->tbv_all->currentRow() != 0)
                        row = ui->tbv_all->currentRow() - 1;
                    else
                        row = 0;
                }
                if (keyEvent->key() == Qt::Key_Down) {
                    if (ui->tbv_all->currentRow() < ui->tbv_all->rowCount() - 1)
                        row = ui->tbv_all->currentRow() + 1;
                    else
                        row = ui->tbv_all->rowCount() - 1;
                }
                ui->ted_comment->setText(allGrades[row].comment);
                return false;
            }
        }
    }
    return false;
}

void MainWindow::on_pbClose_clicked()
{
    this->close();
}

void MainWindow::savePosSize()
{
    settings.setXPos(pos().x());
    settings.setYPos(pos().y());
    settings.setHSize(height());
    settings.setWSize(width());
}

// отрисовка формы
void MainWindow::showEventHelper()
{
    ui->deDate->setDate(QDate::currentDate());
    settings.getSchool(QDate::currentDate());
    timer1->setInterval(settings.getTimeRefresh() * 1000);
    // Таблица классов
    fillTable();
    // Фокус на таблицу
    ui->tbv_all->setFocus();
    fillMainTable(0);
}

void MainWindow::fillMainTable(int curGrade)
{
    // Общая таблица
    ui->tbv_all->setAlternatingRowColors(true);
    ui->tbv_all->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tbv_all->verticalHeader()->hide();
    QStringList columnsNames = {"Класс",
                                "Всего",
                                "Отсутствует",
                                "Болезнь",
                                "Др.причина",
                                "Присутствует",
                               };
    ui->tbv_all->setHorizontalHeaderLabels(columnsNames);
    ui->tbv_all->horizontalHeaderItem(0)->setTextAlignment(Qt::AlignCenter);
    ui->tbv_all->horizontalHeaderItem(1)->setTextAlignment(Qt::AlignCenter);
    ui->tbv_all->horizontalHeaderItem(2)->setTextAlignment(Qt::AlignCenter);
    ui->tbv_all->horizontalHeaderItem(3)->setTextAlignment(Qt::AlignCenter);
    ui->tbv_all->horizontalHeaderItem(4)->setTextAlignment(Qt::AlignCenter);
    ui->tbv_all->horizontalHeaderItem(5)->setTextAlignment(Qt::AlignCenter);
    ui->tbv_all->horizontalHeader()->setStretchLastSection(true);

    if (settings.getShowFullGrid()) {
        ui->tbv_all->setRowCount(allGrades.size()); // количество классов в школе
    } else {
        int i = 0;
        int countRows = 0;
        while (true) {
            if (curGrade == allGrades[i].dig) {
                countRows++;
            }
            i++;
            if (i == allGrades.size()) {
                break;
            }
        }
        ui->tbv_all->setRowCount(countRows); // количество классов в параллели
    }
    // заполняем таблицу данными
    if (settings.getShowFullGrid()) { // полная таблица
        for (int i = 0; i < ui->tbv_all->rowCount(); ++i) {
            ui->tbv_all->setItem(i, 0, new QTableWidgetItem(QString::number(allGrades[i].dig) +
                                                            " \"" + allGrades[i].let.trimmed() + "\"")); // класс
            ui->tbv_all->setItem(i, 1, new QTableWidgetItem(QString::number(allGrades[i].pupil)));                      // всего по списку
            ui->tbv_all->setItem(i, 2, new QTableWidgetItem(QString::number(allGrades[i].absent)));                     // отсутствуют
            ui->tbv_all->setItem(i, 3, new QTableWidgetItem(QString::number(allGrades[i].ill)));                        // по болезни
            ui->tbv_all->setItem(i, 4, new QTableWidgetItem(QString::number(allGrades[i].other)));                      // по другим причинам
            ui->tbv_all->setItem(i, 5, new QTableWidgetItem(QString::number(allGrades[i].pupil - allGrades[i].absent)));// присутствуют
            // Закрашиваем цветом
            for (int j = 0; j < ui->tbv_all->columnCount(); ++j) {
                ui->tbv_all->item(i, j)->setTextAlignment(Qt::AlignCenter);
                if (allGrades[i].done) {
                    ui->tbv_all->item(i, j)->setBackground(QBrush(QColor(0, 255, 0, 127))); // зеленый
                } else {
                    ui->tbv_all->item(i, j)->setBackground(QBrush(QColor(255, 0, 0, 127))); // Красный
                }
            }
        }
    } else { // по параллели
        int count = 0;
        int i = 0;
        while (count < allGrades.size()) {
            if (curGrade == allGrades[count].dig) {
                ui->tbv_all->setItem(i, 0, new QTableWidgetItem(QString::number(allGrades[count].dig) +
                                                               " \"" + allGrades[count].let.trimmed() + "\"")); // класс
                ui->tbv_all->setItem(i, 1, new QTableWidgetItem(QString::number(allGrades[count].pupil)));                      // всего по списку
                ui->tbv_all->setItem(i, 2, new QTableWidgetItem(QString::number(allGrades[count].absent)));                     // отсутствуют
                ui->tbv_all->setItem(i, 3, new QTableWidgetItem(QString::number(allGrades[count].ill)));                        // по болезни
                ui->tbv_all->setItem(i, 4, new QTableWidgetItem(QString::number(allGrades[count].other)));                      // по другим причинам
                ui->tbv_all->setItem(i, 5, new QTableWidgetItem(QString::number(allGrades[count].pupil - allGrades[count].absent)));// присутствуют
            // Закрашиваем цветом
                for (int j = 0; j < ui->tbv_all->columnCount(); ++j) {
                    ui->tbv_all->item(i, j)->setTextAlignment(Qt::AlignCenter);
                    if (allGrades[count].done) {
                        ui->tbv_all->item(i, j)->setBackground(QBrush(QColor(0, 255, 0, 127))); // зеленый
                    } else {
                        ui->tbv_all->item(i, j)->setBackground(QBrush(QColor(255, 0, 0, 127))); // Красный
                    }
                }
                i++;
            }
            count++;
        }
    }
    ui->tbv_all->verticalHeader()->setDefaultSectionSize(ui->tbv_all->verticalHeader()->minimumSectionSize());
    ui->tbv_all->horizontalHeader()->setStretchLastSection(true);
}

void MainWindow::fillTable()
{  
    // label с рабочей датой
    ui->lbWorkDate->setText("<b>" + ui->deDate->date().toString("dd MMMM yyyy") + "</b>");
    if (ui->deDate->date() == QDate::currentDate()) {
        ui->lbWorkDate->setStyleSheet("color: black");
    } else {
        ui->lbWorkDate->setStyleSheet("color: red");
    }
    // Данные по всей школе
    ui->lbAll->setText(QString::number(mySchool.all));
    ui->lbIll->setText(QString::number(mySchool.ill));
    ui->lbOther->setText(QString::number(mySchool.other));
    ui->lbApsent->setText(QString::number(mySchool.ill + mySchool.other));
    ui->lbNow->setText(QString::number(mySchool.all - mySchool.ill - mySchool.other));
    // таблица по параллелям
    ui->tbv_all_2->setColumnCount(6);
    ui->tbv_all_2->setRowCount(gradesParallel.size()); // количество параллелей в школе
    ui->tbv_all_2->setAlternatingRowColors(true);
    ui->tbv_all_2->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tbv_all_2->verticalHeader()->hide();
    QStringList columnsNames2 = {"Параллель:",
                                "Всего: " + QString::number(mySchool.all),
                                "Отсутствует: " + QString::number(mySchool.ill + mySchool.other),
                                "Болезнь: " + QString::number(mySchool.ill),
                                "Др.причина: " + QString::number(mySchool.other),
                                "Присутствует: " + QString::number(mySchool.all - mySchool.ill - mySchool.other),
                               };
    ui->tbv_all_2->setHorizontalHeaderLabels(columnsNames2);
    ui->tbv_all_2->horizontalHeaderItem(0)->setTextAlignment(Qt::AlignCenter);
    ui->tbv_all_2->horizontalHeaderItem(1)->setTextAlignment(Qt::AlignCenter);
    ui->tbv_all_2->horizontalHeaderItem(2)->setTextAlignment(Qt::AlignCenter);
    ui->tbv_all_2->horizontalHeaderItem(3)->setTextAlignment(Qt::AlignCenter);
    ui->tbv_all_2->horizontalHeaderItem(4)->setTextAlignment(Qt::AlignCenter);
    ui->tbv_all_2->horizontalHeaderItem(5)->setTextAlignment(Qt::AlignCenter);
    ui->tbv_all_2->horizontalHeader()->setStretchLastSection(true);
    ui->tbv_all_2->verticalHeader()->setDefaultSectionSize(ui->tbv_all_2->verticalHeader()->minimumSectionSize());
    ui->tbv_all_2->horizontalHeader()->setStretchLastSection(true);

    for (int i = 0; i < ui->tbv_all_2->rowCount(); ++i) {
           ui->tbv_all_2->setItem(i, 0, new QTableWidgetItem(QString::number(gradesParallel[i].dig)));                        // Параллель
           ui->tbv_all_2->setItem(i, 1, new QTableWidgetItem(QString::number(gradesParallel[i].pupil)));                      // всего по списку
           ui->tbv_all_2->setItem(i, 2, new QTableWidgetItem(QString::number(gradesParallel[i].absent)));                     // отсутствуют
           ui->tbv_all_2->setItem(i, 3, new QTableWidgetItem(QString::number(gradesParallel[i].ill)));                        // по болезни
           ui->tbv_all_2->setItem(i, 4, new QTableWidgetItem(QString::number(gradesParallel[i].other)));                      // по другим причинам
           ui->tbv_all_2->setItem(i, 5, new QTableWidgetItem(QString::number(gradesParallel[i].pupil - gradesParallel[i].absent)));// присутствуют
           for (int j = 0; j < 6; j++) {
               ui->tbv_all_2->item(i, j)->setTextAlignment(Qt::AlignCenter);
               if (gradesParallel[i].done) {
                   ui->tbv_all_2->item(i, j)->setBackground(QBrush(QColor(0, 255, 0, 127))); // зеленый
               }
           }
    }
    // Устанавливаем Контекстное Меню
    ui->tbv_all->setContextMenuPolicy(Qt::CustomContextMenu);
    // Подключаем СЛОТ вызова контекстного меню
    connect(ui->tbv_all, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(slotCustomMenuRequested(QPoint)));
    ui->tbv_all->setColumnCount(6);
}

// кнопка настройки
void MainWindow::on_tb_options_clicked()
{
    Options w_opt;
    w_opt.setWindowTitle("Настройки");   
    w_opt.exec();
    timer1->setInterval(settings.getTimeRefresh() * 1000);
}

// Редактировать общие данные по классу
void MainWindow::slotEditRecordMain()
{
    //Выясняем, какая из строк была выбрана
    int row = ui->tbv_all->selectionModel()->currentIndex().row();
    if(row >= 0){
        mySchool.currentID = allGrades[row].id;
        QString strGrade = QString::number(allGrades[row].dig) + "\"" + allGrades[row].let + "\"";
        //Редактируем
        editGrade = true;
        currentGrade = allGrades[row];
        EditMain w_edm;
        w_edm.setWindowTitle("Общие данные по " + strGrade);
        w_edm.exec();
    }
    fillTable();
    fillMainTable(0);
}

// Добавить класс
void MainWindow::slotAddRecord()
{
    editGrade = false;
    EditMain w_edm;
    w_edm.setWindowTitle("Добавление класса");
    w_edm.exec();
    fillTable();
    fillMainTable(0);
}

// редактировать данные об отсутствующих
void MainWindow::slotEditRecordTruants()
{
    //Выясняем, какая из строк была выбрана
    int row = ui->tbv_all->selectionModel()->currentIndex().row();
    if(row >= 0){
        mySchool.currentID = allGrades[row].id;
        QString strGrade = QString::number(allGrades[row].dig) + "\"" + allGrades[row].let + "\"";
        //Редактируем
        currentGrade = allGrades[row];
        EditTruants w_edt;
        w_edt.setWindowTitle("Отсутствующие по " + strGrade + " на " + mySchool.wDate.toString("dd.MM.yyyy"));
        w_edt.exec();
    }
    fillTable();
}

// Удалить класс
void MainWindow::slotRemoveRecord()
{
    //Выясняем, какая из строк была выбрана
    int row = ui->tbv_all->selectionModel()->currentIndex().row();
    if(row >= 0){
        mySchool.currentID = allGrades[row].id;
        QString strGrade = QString::number(allGrades[row].dig) + "\"" + allGrades[row].let + "\"";
        if (QMessageBox::warning(this, FULL_PROGRAMM_NAME, "Вы уверены, что хотите УДАЛИТЬ данные по " + strGrade + " ?",
                                 QMessageBox::Yes | QMessageBox::No) == QMessageBox::No)
        {
            return;
        } else {
            if (QMessageBox::warning(this, FULL_PROGRAMM_NAME, "Данные по " + strGrade +
                                     " будут БЕЗВОЗВРАТНО ПОТЕРЯНЫ!!!\n\n"
                                     "Берете ОТВЕТСТВЕННОСТЬ за изменение данных НА СЕБЯ?",
                                     QMessageBox::Yes | QMessageBox::No) == QMessageBox::No)
            {
                return;
            } else {
                if (settings.deleteGrade(allGrades[row].id))
                    QMessageBox::information(nullptr, "Информация", "Класс " + strGrade + " удален!");
                else
                    QMessageBox::critical(nullptr, "Ошибка", "Не смог удалить");
            }
        }
    }
    fillTable();
    fillMainTable(0);
}

void MainWindow::slotCustomMenuRequested(QPoint pos)
{
    /* Создаем объект контекстного меню */
    QMenu * menu = new QMenu(this);
    /* Создаём действия для контекстного меню */
    QAction * editGradeMain = new QAction("Редактировать общее количество учеников", this);
    QAction * editGradeTruants = new QAction("Редактировать данные об остутствующих", this);
    QAction * addGrade = new QAction("Добавить класс", this);
    QAction * deleteGrade = new QAction("Удалить класс", this);
    /* Подключаем СЛОТы обработчики для действий контекстного меню */
    connect(editGradeMain, SIGNAL(triggered()), this, SLOT(slotEditRecordMain()));  // Обработчик вызова диалога редактирования
    connect(editGradeTruants, SIGNAL(triggered()), this, SLOT(slotEditRecordTruants()));  // Обработчик вызова диалога редактирования
    connect(addGrade, SIGNAL(triggered()), this, SLOT(slotAddRecord())); // Обработчик добавления записи
    connect(deleteGrade, SIGNAL(triggered()), this, SLOT(slotRemoveRecord())); // Обработчик удаления записи
    /* Устанавливаем действия в меню */
    menu->addAction(editGradeMain);
    menu->addAction(editGradeTruants);
    menu->addSeparator();
    menu->addAction(addGrade);
    menu->addSeparator();
    menu->addAction(deleteGrade);
    /* Вызываем контекстное меню */
    menu->popup(ui->tbv_all->viewport()->mapToGlobal(pos));
}

void MainWindow::on_pushButton_2_clicked()
{
    QString strText = "Программа учета отсутствующих в школе по классам\n\n(с) 2018-2020 Черевко Егор"
                      "\nМОБУ СОШ Агалатовский ЦО\n\nС++, Qt " + QString(qVersion());
    if (ui->lbLic->isVisible()) {
        strText = strText + "\n\nЛицензия:\n" + ui->lbLic->text();
    }
    QMessageBox::about(this, FULL_PROGRAMM_NAME, strText);
}

void MainWindow::on_pbRefresh_clicked()
{
    settings.getSchool(ui->deDate->date());
    fillTable();
    fillMainTable(0);
    ui->tbv_all->setFocus();
}

void MainWindow::on_deDate_userDateChanged()
{
    settings.getSchool(ui->deDate->date());
    fillTable();
    fillMainTable(0);
    ui->tbv_all->setFocus();
}

void MainWindow::on_tbv_all_cellClicked(int row)
{
    ui->ted_comment->setText(allGrades[row].comment);
}

void MainWindow::on_deDate_dateChanged(const QDate)
{
    ui->deDate->setMaximumDate(QDate::currentDate());
}

void MainWindow::on_tbv_all_cellDoubleClicked(int row)
{
    //Выясняем, какая из строк была выбрана
    if(row >= 0){
        mySchool.currentID = allGrades[row].id;
        QString strGrade = QString::number(allGrades[row].dig) + "\"" + allGrades[row].let + "\"";
        //Редактируем
        currentGrade = allGrades[row];
        EditTruants w_edt;
        w_edt.setWindowTitle("Отсутствующие по " + strGrade + " на " + mySchool.wDate.toString("dd.MM.yyyy"));
        w_edt.exec();
    }
    fillTable();
}

void MainWindow::checkActivation()
{
    // skip..
}

void MainWindow::checkLic()
{
    // skip..
}

QString crypto(QString inStr, bool doCript)
{
    QString outStr = "";
    // skip..
    return outStr;
}

QStringList MainWindow::checkLicExp()
{
    QStringList result;
    // skip...
    return result;
}


void MainWindow::on_pushButton_clicked()
{
    QString pathOut = settings.getPathOut();
    if ((pathOut.length() == 0) || (!QDir(pathOut).exists())) {
        QMessageBox::warning(nullptr, FULL_PROGRAMM_NAME, "Неверно указана папка для выгрузки отчетов!");
        return;
    }
    QProgressDialog progress;
    progress.setLabelText("Выгрузка данных...");
    progress.setWindowModality(Qt::WindowModal);
    progress.setMinimum(0);
    progress.setMaximum(11);
    progress.setValue(0);
    progress.show();
    QString fName = pathOut + "/" + "otchet_" + mySchool.wDate.toString("dd-MM-yyyy") + ".xlsx";
    QXlsx::Document xlsx;
    xlsx.addSheet(mySchool.wDate.toString("dd MMMM yyyy"));
    QStringList listRun = infoRun.split(",");
    xlsx.write(1, 1, listRun[2]);
    xlsx.write(2, 1, "Данные об отсутствующих на " + mySchool.wDate.toString("dd MMMM yyyy"));
    xlsx.write(3, 1, "Всего учеников: ");
    xlsx.write(3, 2, mySchool.all);
    xlsx.write(4, 1, "Отсутствуют: ");
    xlsx.write(4, 2, mySchool.ill + mySchool.other);
    xlsx.write(5, 1, "По болезни: ");
    xlsx.write(5, 2, mySchool.ill);
    xlsx.write(6, 1, "По другой причине: ");
    xlsx.write(6, 2, mySchool.other);
    xlsx.write(7, 1, "Присутствуют: ");
    xlsx.write(7, 2, mySchool.all - mySchool.ill - mySchool.other);
    int rowTableStart = 10;
    int k = 0;
    xlsx.write(rowTableStart, ++k, "Параллель");
    xlsx.write(rowTableStart, ++k, "Всего по списку");
    xlsx.write(rowTableStart, ++k, "Отсутствуют");
    xlsx.write(rowTableStart, ++k, "По болезни");
    xlsx.write(rowTableStart, ++k, "По другим причинам");
    xlsx.write(rowTableStart, ++k, "Присутствуют");
    xlsx.setColumnWidth(1, 15);
    xlsx.setColumnWidth(2, 15);
    xlsx.setColumnWidth(3, 15);
    xlsx.setColumnWidth(4, 15);
    xlsx.setColumnWidth(5, 15);
    xlsx.setColumnWidth(6, 15);
    int i = 0;
    for (int row = ++rowTableStart; row < gradesParallel.size() + rowTableStart; ++row) {
        int j = 0;
        xlsx.write(row, ++j, gradesParallel[i].dig);      // Параллель
        xlsx.write(row, ++j, gradesParallel[i].pupil);    // всего по списку
        xlsx.write(row, ++j, gradesParallel[i].absent);   // отсутствуют
        xlsx.write(row, ++j, gradesParallel[i].ill);      // по болезни
        xlsx.write(row, ++j, gradesParallel[i].other);    // по другим причинам
        xlsx.write(row, ++j, gradesParallel[i].pupil - gradesParallel[i].absent);    // присутствуют
        i++;
        progress.setValue(i);
        QEventLoop loop;
        QTimer::singleShot(100, &loop, &QEventLoop::quit);
        loop.exec();
    }
    progress.cancel();
    if (xlsx.saveAs(fName)) {
       if (!QDesktopServices::openUrl(QUrl::fromLocalFile(fName))) {
           QMessageBox::information(nullptr, FULL_PROGRAMM_NAME, "Данные выгружены в файл: " + fName);
       }
    } else {
        QMessageBox::warning(nullptr, FULL_PROGRAMM_NAME, "Не удалось выгрузить данные!");
    }
}
