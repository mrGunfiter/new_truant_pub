#ifndef INFO_H
#define INFO_H

#include <QDialog>
#include "../defines.h"

namespace Ui {
class Info;
}

class Info : public QDialog
{
    Q_OBJECT

public:
    explicit Info(QWidget *parent = nullptr);
    ~Info();
    QString info;

protected:
    void showEvent(QShowEvent *ev);

private slots:
    void on_buttonBox_accepted();

private:
    void showEventHelper();
    Ui::Info *ui;
};

#endif // INFO_H
