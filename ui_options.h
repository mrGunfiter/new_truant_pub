/********************************************************************************
** Form generated from reading UI file 'options.ui'
**
** Created by: Qt User Interface Compiler version 5.9.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OPTIONS_H
#define UI_OPTIONS_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QToolButton>

QT_BEGIN_NAMESPACE

class Ui_Options
{
public:
    QDialogButtonBox *buttonBox;
    QCheckBox *cbPosition;
    QCheckBox *cbSize;
    QCheckBox *cbQuit;
    QLabel *label_2;
    QLineEdit *lePathOut;
    QToolButton *tbPathOut;
    QLabel *label_3;
    QLineEdit *leFile2Base;
    QToolButton *tbFile2Base;
    QGroupBox *groupBox;
    QDateEdit *deDateBegin;
    QDateEdit *deDateEnd;
    QLabel *label_4;
    QLabel *label_5;
    QPushButton *pbLoad;
    QLabel *lbTimer;
    QSpinBox *sbTimer;
    QGroupBox *groupBox_2;
    QRadioButton *rbFull;
    QRadioButton *rbParallel;

    void setupUi(QDialog *Options)
    {
        if (Options->objectName().isEmpty())
            Options->setObjectName(QStringLiteral("Options"));
        Options->resize(423, 450);
        Options->setMinimumSize(QSize(423, 450));
        Options->setMaximumSize(QSize(423, 450));
        buttonBox = new QDialogButtonBox(Options);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setGeometry(QRect(10, 397, 401, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        cbPosition = new QCheckBox(Options);
        cbPosition->setObjectName(QStringLiteral("cbPosition"));
        cbPosition->setGeometry(QRect(30, 20, 251, 21));
        cbSize = new QCheckBox(Options);
        cbSize->setObjectName(QStringLiteral("cbSize"));
        cbSize->setGeometry(QRect(30, 50, 181, 21));
        cbQuit = new QCheckBox(Options);
        cbQuit->setObjectName(QStringLiteral("cbQuit"));
        cbQuit->setGeometry(QRect(30, 80, 291, 21));
        label_2 = new QLabel(Options);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(30, 220, 160, 16));
        lePathOut = new QLineEdit(Options);
        lePathOut->setObjectName(QStringLiteral("lePathOut"));
        lePathOut->setGeometry(QRect(190, 217, 190, 23));
        lePathOut->setReadOnly(true);
        tbPathOut = new QToolButton(Options);
        tbPathOut->setObjectName(QStringLiteral("tbPathOut"));
        tbPathOut->setGeometry(QRect(380, 217, 23, 22));
        label_3 = new QLabel(Options);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(25, 280, 160, 16));
        leFile2Base = new QLineEdit(Options);
        leFile2Base->setObjectName(QStringLiteral("leFile2Base"));
        leFile2Base->setGeometry(QRect(184, 277, 190, 23));
        tbFile2Base = new QToolButton(Options);
        tbFile2Base->setObjectName(QStringLiteral("tbFile2Base"));
        tbFile2Base->setGeometry(QRect(374, 277, 23, 22));
        groupBox = new QGroupBox(Options);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(10, 247, 401, 131));
        deDateBegin = new QDateEdit(groupBox);
        deDateBegin->setObjectName(QStringLiteral("deDateBegin"));
        deDateBegin->setGeometry(QRect(190, 60, 110, 26));
        deDateEnd = new QDateEdit(groupBox);
        deDateEnd->setObjectName(QStringLiteral("deDateEnd"));
        deDateEnd->setGeometry(QRect(190, 91, 110, 26));
        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(17, 65, 151, 17));
        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(17, 95, 160, 17));
        pbLoad = new QPushButton(groupBox);
        pbLoad->setObjectName(QStringLiteral("pbLoad"));
        pbLoad->setGeometry(QRect(310, 90, 80, 25));
        lbTimer = new QLabel(Options);
        lbTimer->setObjectName(QStringLiteral("lbTimer"));
        lbTimer->setGeometry(QRect(30, 110, 251, 16));
        sbTimer = new QSpinBox(Options);
        sbTimer->setObjectName(QStringLiteral("sbTimer"));
        sbTimer->setGeometry(QRect(300, 106, 80, 24));
        sbTimer->setMinimum(1);
        sbTimer->setMaximum(300);
        sbTimer->setSingleStep(1);
        //sbTimer->setStepType(QAbstractSpinBox::DefaultStepType);
        sbTimer->setValue(5);
        sbTimer->setDisplayIntegerBase(10);
        groupBox_2 = new QGroupBox(Options);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        groupBox_2->setGeometry(QRect(10, 140, 401, 60));
        rbFull = new QRadioButton(groupBox_2);
        rbFull->setObjectName(QStringLiteral("rbFull"));
        rbFull->setGeometry(QRect(20, 30, 180, 23));
        rbFull->setMinimumSize(QSize(180, 0));
        rbFull->setMaximumSize(QSize(180, 16777215));
        rbParallel = new QRadioButton(groupBox_2);
        rbParallel->setObjectName(QStringLiteral("rbParallel"));
        rbParallel->setGeometry(QRect(210, 30, 180, 23));
        rbParallel->setMinimumSize(QSize(180, 0));
        rbParallel->setMaximumSize(QSize(180, 16777215));
        rbParallel->setCheckable(false);
        groupBox->raise();
        buttonBox->raise();
        cbPosition->raise();
        cbSize->raise();
        cbQuit->raise();
        label_2->raise();
        lePathOut->raise();
        tbPathOut->raise();
        label_3->raise();
        leFile2Base->raise();
        tbFile2Base->raise();
        lbTimer->raise();
        sbTimer->raise();
        groupBox_2->raise();

        retranslateUi(Options);
        QObject::connect(buttonBox, SIGNAL(accepted()), Options, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), Options, SLOT(reject()));

        QMetaObject::connectSlotsByName(Options);
    } // setupUi

    void retranslateUi(QDialog *Options)
    {
        Options->setWindowTitle(QApplication::translate("Options", "Dialog", Q_NULLPTR));
        cbPosition->setText(QApplication::translate("Options", "\320\227\320\260\320\277\320\276\320\274\320\270\320\275\320\260\321\202\321\214 \320\277\320\276\320\273\320\276\320\266\320\265\320\275\320\270\320\265", Q_NULLPTR));
        cbSize->setText(QApplication::translate("Options", "\320\227\320\260\320\277\320\276\320\274\320\270\320\275\320\260\321\202\321\214 \321\200\320\260\320\267\320\274\320\265\321\200 \320\276\320\272\320\275\320\260", Q_NULLPTR));
        cbQuit->setText(QApplication::translate("Options", "\320\235\320\265 \321\201\320\277\321\200\320\260\321\210\320\270\320\262\320\260\321\202\321\214 \320\277\321\200\320\270 \320\267\320\260\320\262\320\265\321\200\321\210\320\265\320\275\320\270\320\270 \321\200\320\260\320\261\320\276\321\202\321\213", Q_NULLPTR));
        label_2->setText(QApplication::translate("Options", "\320\237\321\203\321\202\321\214 \320\262\321\213\320\263\321\200\321\203\320\267\320\272\320\270 \320\276\321\202\321\207\320\265\321\202\320\276\320\262:", Q_NULLPTR));
        tbPathOut->setText(QApplication::translate("Options", "...", Q_NULLPTR));
        label_3->setText(QApplication::translate("Options", "\320\244\320\260\320\271\320\273 \320\264\320\273\321\217 \320\267\320\260\320\263\321\200\321\203\320\267\320\272\320\270:", Q_NULLPTR));
        tbFile2Base->setText(QApplication::translate("Options", "...", Q_NULLPTR));
        groupBox->setTitle(QApplication::translate("Options", "\320\227\320\260\320\263\321\200\321\203\320\267\320\272\320\260 \320\264\320\260\320\275\320\275\321\213\321\205 \320\277\320\276 \320\272\320\273\320\260\321\201\321\201\320\260\320\274 \320\275\320\260 \320\275\320\260\321\207\320\260\320\273\320\276 \320\263\320\276\320\264\320\260", Q_NULLPTR));
        label_4->setText(QApplication::translate("Options", "\320\235\320\260\321\207\320\260\320\273\320\276 \321\203\321\207\320\265\320\261\320\275\320\276\320\263\320\276 \320\263\320\276\320\264\320\260:", Q_NULLPTR));
        label_5->setText(QApplication::translate("Options", "\320\236\320\272\320\276\320\275\321\207\320\260\320\275\320\270\320\265 \321\203\321\207\320\265\320\261\320\275\320\276\320\263\320\276 \320\263\320\276\320\264\320\260:", Q_NULLPTR));
        pbLoad->setText(QApplication::translate("Options", "\320\227\320\260\320\263\321\200\321\203\320\267\320\270\321\202\321\214", Q_NULLPTR));
        lbTimer->setText(QApplication::translate("Options", "\320\230\320\275\321\202\320\265\321\200\320\262\320\260\320\273 \320\276\320\261\320\275\320\276\320\262\320\273\320\265\320\275\320\270\321\217", Q_NULLPTR));
        sbTimer->setSuffix(QApplication::translate("Options", " \321\201\320\265\320\272", Q_NULLPTR));
        groupBox_2->setTitle(QApplication::translate("Options", "\320\222\320\275\320\265\321\210\320\275\320\270\320\271 \320\262\320\270\320\264 \321\202\320\260\320\261\320\273\320\270\321\206\321\213 \320\272\320\273\320\260\321\201\321\201\320\276\320\262:", Q_NULLPTR));
        rbFull->setText(QApplication::translate("Options", "\320\237\320\276\320\273\320\275\321\213\320\271", Q_NULLPTR));
        rbParallel->setText(QApplication::translate("Options", "\320\237\320\276 \320\277\320\260\321\200\320\260\320\273\320\273\320\265\320\273\321\217\320\274", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class Options: public Ui_Options {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_OPTIONS_H
