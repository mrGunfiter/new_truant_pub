#include <QDebug>
#include <QMessageBox>
#include "edittruants.h"
#include "ui_edittruants.h"
#include "../defines.h"
#include "../settings.h"

extern Grade currentGrade;
extern School mySchool;
extern QVector <Grade> allGrades;
extern bool editGrade;
extern Settings settings;

EditTruants::EditTruants(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EditTruants)
{
    ui->setupUi(this);
}

EditTruants::~EditTruants()
{
    delete ui;
}

void EditTruants::showEvent(QShowEvent *ev)
{
    QDialog::showEvent(ev);
    showEventHelper();
}

void EditTruants::showEventHelper()
{
    ui->lbMain->setText("<b>" + this->windowTitle() + "</b>");
    ui->ill_absent->setValue(currentGrade.ill);
    ui->ather_absent->setValue(currentGrade.other);
    ui->notes->setText(currentGrade.comment);
}

// Изменение количества отсутствующих в классе
void EditTruants::on_buttonBox_accepted()
{
    if (!settings.updateTruants(currentGrade.id, mySchool.wDate, ui->ill_absent->value(),
                                ui->ather_absent->value(), ui->notes->toPlainText().trimmed())) {
         QMessageBox::critical(nullptr, FULL_PROGRAMM_NAME, "Не удалось записать!");
    }
}
