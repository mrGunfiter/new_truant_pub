#include "settings.h"
#include <QDebug>
#include <QFile>

extern School mySchool;
extern QVector <Grade> gradesParallel;
extern QVector <Grade> allGrades;

Settings::Settings(QString fileName):
    fileNameIni(fileName),
    iniFileExists(false),
    posWindow(false),
    sizeWindow(false),
    showFullGrid(true),
    timeRefresh(120)
{
    this->setIniFileExists(QFile::exists(fileNameIni));
    if (QDate::currentDate().month() > 6) {
        dateStart.setDate(QDate::currentDate().year(), 9, 1);
        dateEnd.setDate(QDate::currentDate().year() + 1, 5, 31);
    } else {
        dateStart.setDate(QDate::currentDate().year() - 1, 9, 1);
        dateEnd.setDate(QDate::currentDate().year(), 5, 31);
    }
}

bool Settings::getIniFileExists() const
{
    return iniFileExists;
}

void Settings::setIniFileExists(bool value)
{
    iniFileExists = value;
}

bool Settings::readConfig()
{
    QSettings options(fileNameIni, QSettings::IniFormat);
    QStringList groups = options.childGroups();
    if ((options.contains("connection/host")) && (options.contains("connection/port")) && (options.contains("connection/type"))) {
        options.beginGroup("connection");
        Settings::host = options.value("host").toString();
        Settings::port = options.value("port").toInt(/*0,10*/);
        QString strTypebase = options.value("type").toString();
        if (strTypebase.toLower() == "mysql" )
            Settings::typeBD = MSQL;
        else if (strTypebase.toLower() == "postgresql" )
            Settings::typeBD = PSQL;
        options.endGroup();
        options.beginGroup("lastuse");
        Settings::lastDigit = options.value("lastDigit").toInt();
        Settings::lastLetter = options.value("lastLetter").toString();
        options.endGroup();
        options.beginGroup("options");
        setQuitNoAsk(options.value("quitAsk").toBool());
        setSizeWindow(options.value("sizeWindow").toBool());
        setPosWindow(options.value("posWindow").toBool());
        if (getPosWindow()) {
            setXPos(options.value("xPos").toInt());
            setYPos(options.value("yPos").toInt());
        }
        if (getSizeWindow()) {
            setHSize(options.value("hSize").toInt());
            setWSize(options.value("wSize").toInt());
        }
        setPathOut(options.value("pathOut").toString());
        // TODO: setShowFullGrid(options.value("showGridFull").toBool());
        setShowFullGrid(true);
        setTimeRefresh(options.value("timeRefresh").toInt());
        options.endGroup();
        return true;
    } else
        return false;

}

bool Settings::writeConfig()
{
    QSettings options(fileNameIni, QSettings::IniFormat);
   // Запись в файл
    options.beginGroup("connection");   
    if (Settings::typeBD == MSQL) {
        options.setValue("type", "MySQL");
    }
    else if (Settings::typeBD == PSQL) {
        options.setValue("type", "PostgreSQL");
    }
    options.setValue("host", Settings::host);
    options.setValue("port", Settings::port);
    options.endGroup();
    options.beginGroup("lastuse");
    options.setValue("lastDigit", Settings::lastDigit);
    options.setValue("lastLetter", Settings::lastLetter);
    options.endGroup();
    options.beginGroup("options");
    options.setValue("quitAsk", getQuitNoAsk());
    options.setValue("sizeWindow", getSizeWindow());
    options.setValue("posWindow", getPosWindow());
    if (getPosWindow()) {
        options.setValue("xPos", getXPos());
        options.setValue("yPos", getYPos());
    }
    if (getSizeWindow()) {
        options.setValue("hSize", getHSize());
        options.setValue("wSize", getWSize());
    }
    options.setValue("pathOut", getPathOut());
    options.setValue("showGridFull", getShowFullGrid());
    options.setValue("timeRefresh", getTimeRefresh());
    options.endGroup();
    // TODO: возвращать true или false в результате успешной или неуспешной записи в файл
    if ((options.contains("connection/host")) && (options.contains("connection/port")) && (options.contains("connection/type"))) {
        return true;
    }
    return false;
}

//Проверка наличия БД и подключения к ней
int Settings::checkBase()
{
    int returnCode = 0;
    db.setHostName(host);
    db.setPort(port);
    db.setDatabaseName(baseName);
    db.setUserName(user);
    db.setPassword(password);
    db.setConnectOptions();
    if (db.open()) {
        returnCode = 0;
    } else {
        QString baseError = db.lastError().databaseText();
        if (baseError.contains(Settings::host, Qt::CaseInsensitive)) {
            returnCode = 1; // Нет подключения к серверу
        } else if (baseError.contains("truant", Qt::CaseInsensitive)) {
            returnCode = 2; // На сервере нет рабочей БД
        } else if (baseError.contains("driver not loaded", Qt::CaseInsensitive)) {
            returnCode = 3; // Не найден драйвер БД
        }
    }
    return returnCode;
}

// Создание БД
bool Settings::createBase()
{
    QCoreApplication::processEvents();
    QString strCreateBase = "";
    QString strCreateTablesUser = "";
    if (Settings::typeBD == MSQL) {
        db.setDatabaseName("sys");
        strCreateBase = "CREATE DATABASE `%1`; \
                        USE %1;\
               CREATE TABLE `system` ( \
                       `date_start` DATE NOT NULL, \
                       `build` VARCHAR(10) NOT NULL, \
                       `info` VARCHAR(120) NOT NULL, \
                       `info2` VARCHAR(60) NOT NULL, \
                       PRIMARY KEY (`date_start`) \
                       )  ENGINE=InnoDB DEFAULT CHARSET=utf8; \
               CREATE TABLE `school` ( \
                        `id` int(11) NOT NULL AUTO_INCREMENT, \
                        `dig` tinyint(1) NOT NULL, \
                        `let` varchar(3) CHARACTER SET utf8 NOT NULL, \
                        `date_start` date NOT NULL, \
                        `date_end` date NOT NULL, \
                        PRIMARY KEY (`id`), \
                        UNIQUE KEY `id_UNIQUE` (`id`) \
                ) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8 COLLATE=utf8_bin; \
                        CREATE TABLE `pupils` ( \
                          `id` int(11) NOT NULL, \
                          `count` tinyint(50) NOT NULL, \
                          `date` date NOT NULL, \
                          PRIMARY KEY (`id`,`date`), \
                          CONSTRAINT `id_pupils` FOREIGN KEY (`id`) REFERENCES `school` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION \
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8; \
                CREATE TABLE `truants` ( \
                        `id` int(11) NOT NULL, \
                        `date` date NOT NULL, \
                        `ill` tinyint(50) DEFAULT NULL, \
                        `other` tinyint(50) DEFAULT NULL, \
                        `comp` varchar(45) CHARACTER SET big5 COLLATE big5_bin NOT NULL, \
                        `time` datetime(1) NOT NULL, \
                        `comment` varchar(255) COLLATE utf8_bin DEFAULT NULL, \
                        PRIMARY KEY (`id`,`date`), \
                        KEY `id_idx` (`id`), \
                        CONSTRAINT `id_truants` FOREIGN KEY (`id`) REFERENCES `school` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION \
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin; \
                CREATE USER '%2'@'%' IDENTIFIED BY '%3'; \
                GRANT ALL PRIVILEGES ON `%1`.* TO '%2'@'%'; \
                FLUSH PRIVILEGES;";
                strCreateBase = strCreateBase.arg(baseName).arg(user).arg(password);
    } else if (Settings::typeBD == PSQL) {
        db.setDatabaseName("postgres");
        strCreateBase = "CREATE DATABASE %1;";
        strCreateTablesUser = "CREATE TABLE %1.public.school (\
                    id serial NOT NULL, \
                    dig smallint NOT NULL, \
                    let character(3) COLLATE pg_catalog.\"default\" NOT NULL, \
                    date_start date NOT NULL, \
                    date_end date NOT NULL, \
                    CONSTRAINT school_pkey PRIMARY KEY (id) \
                ); \
                CREATE TABLE %1.public.pupils (\
                    id integer NOT NULL, \
                    count integer NOT NULL, \
                    date date NOT NULL, \
                    CONSTRAINT id_idx FOREIGN KEY (id) \
                        REFERENCES %1.public.school (id) MATCH SIMPLE \
                        ON UPDATE NO ACTION \
                        ON DELETE NO ACTION \
                ); \
                CREATE TABLE %1.public.truants (\
                    id integer NOT NULL, \
                    date date NOT NULL, \
                    ill smallint, \
                    other smallint, \
                    comp character(45) COLLATE pg_catalog.\"default\" NOT NULL, \
                    \"time\" timestamp without time zone NOT NULL, \
                    comment character(255) COLLATE pg_catalog.\"default\", \
                    CONSTRAINT id_idx FOREIGN KEY (id) \
                        REFERENCES %1.public.school (id) MATCH SIMPLE \
                        ON UPDATE NO ACTION \
                        ON DELETE NO ACTION \
                ); \
                CREATE USER %2 WITH password '%3'; \
                GRANT ALL privileges ON DATABASE %1 TO %2;\
                GRANT ALL PRIVILEGES ON %1.public.school TO %2; \
                GRANT ALL PRIVILEGES ON %1.public.pupils TO %2; \
                GRANT ALL PRIVILEGES ON %1.public.truants TO %2;\
                GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public TO %2;";
                strCreateBase = strCreateBase.arg(baseName);
                strCreateTablesUser = strCreateTablesUser.arg(baseName).arg(user).arg(password);
    }
    db.setHostName(host);
    db.setPort(port);
    db.setUserName(superUser);
    db.setPassword(superPassword);
    db.setConnectOptions();
    QCoreApplication::processEvents();
    QSqlQuery query(db);
    if (db.open()) {
        QCoreApplication::processEvents();
        query.prepare(strCreateBase);
        if (!query.exec()) {
            qDebug() << query.lastError().text();
            return false;
        } else {
            if (Settings::typeBD == PSQL) {
                db.close();
                db.setDatabaseName(baseName);
                db.setHostName(host);
                db.setPort(port);
                db.setUserName(superUser);
                db.setPassword(superPassword);
                db.setConnectOptions();
                if (db.open()) {
                    QCoreApplication::processEvents();
                    query.prepare(strCreateTablesUser);
                    if (!query.exec()) {
                        qDebug() << query.lastError().text();
                        return false;
                    }
                }
            }
        }
        return true;
    }
    return false;
}

bool Settings::loadFile2Base(QDate dateStart, QDate dateEnd, QString fileToBase)
{
    QFile file(fileToBase);
    if ( !file.open(QFile::ReadOnly | QFile::Text) ) {
         qDebug() << "File not exists";
         return false;
    } else {
        QTextStream in(&file);
        while (!in.atEnd())
        {
            QCoreApplication::processEvents();
            QString line = in.readLine();
            QStringList list = line.split(";");
            QSqlQuery query(db);
            Grade tmpGrade;
            tmpGrade.dig = list[0].toInt();
            tmpGrade.let = list[1];
            tmpGrade.pupil = list[2].toInt();
            if (!addGrade(tmpGrade, dateStart, dateEnd, dateStart)) {
                qDebug() << "ERROR: couldn't add a grade!";
                return false;
            }
        }
        file.close();
    }
    return true;
}

// Получаем данные по классу за дату из БД
Grade Settings::getGrade(QDate wDate, int gDig, QString gLet)
{
    Grade result;
    if (flagBusy) {
        return result;
    }
    flagBusy = true;
    QString gLetter;
    #ifdef Q_OS_WIN
        int i = codesLetters.winCodes.indexOf(gLet);
        if (i != -1) gLetter = codesLetters.utfCodes[i];
    #endif
    #ifdef Q_OS_LINUX
        gLetter = gLet;
    #endif
    #ifdef Q_OS_MAC
        gLetter = gLet;
    #endif    
    QSqlQuery query(db);
    QString strGet;
    strGet = "SELECT id FROM %1 WHERE dig = :dig and let = :let and date_start < :date and date_end > :date";
    strGet = strGet.arg(getTryTableName("school"));
    if (db.open()) {
        // Получаем ID
        query.prepare(strGet);
        query.bindValue(":dig", gDig);
        query.bindValue(":let", gLetter);
        query.bindValue(":date", wDate.toString("yyyy-MM-dd"));
        if (!query.exec()) {
            qDebug() << query.lastError().text();
        } else {
            if (query.next()) {
                QSqlRecord rec = query.record();
                result.id = query.value(0).toInt();
            }
        }
        // Получаем данные по классу итого
        query.clear();       
        strGet = "SELECT count FROM %1 WHERE id = :id and date <= :date";
        strGet = strGet.arg(getTryTableName("pupils"));
        query.prepare(strGet);
        query.bindValue(":id", result.id);
        query.bindValue(":date", wDate.toString("yyyy-MM-dd"));
        if (!query.exec()) {
            qDebug() << query.lastError().text();
        } else {
            if (query.next()) {
                QSqlRecord rec = query.record();
                result.pupil = query.value(rec.indexOf("count")).toInt();
            }
        }
        // Получаем данные об остутствующих
        query.clear();
        strGet = "SELECT ill, other, comment FROM %1 WHERE id = :id and date = :date";
        strGet = strGet.arg(getTryTableName("truants"));
        query.prepare(strGet);
        query.bindValue(":id", result.id);
        query.bindValue(":date", wDate.toString("yyyy-MM-dd"));
        if (!query.exec()) {
            qDebug() << query.lastError().text();
        } else {
            if (query.next()) {
                QSqlRecord rec = query.record();
                result.ill = query.value(rec.indexOf("ill")).toInt();
                result.other = query.value(rec.indexOf("other")).toInt();
                result.comment = query.value(rec.indexOf("comment")).toString();
                if (query.size() != 0) {
                    result.done = true;
                }
            }
        }
        result.absent = result.ill + result.other;
    } else {
        qDebug() << "Base not open!";
    }
    flagBusy = false;
    return result;
}

// Получение данных из таблицы SCHOOL и TRUANTS по ВСЕЙ школе
void Settings::getSchool(QDate wDate)
{
    if (flagBusy) {
        return;
    }
    QSqlQuery query(db);
    flagBusy = true;
    allGrades.clear();
    mySchool.Clear();
    mySchool.wDate = wDate;
    QString strGet = "SELECT id, dig, let FROM %1 WHERE date_start < :date and date_end > :date ORDER BY dig, let";
    strGet = strGet.arg(getTryTableName("school"));
    if (db.open()) {
        // ID цифра буква
        query.prepare(strGet);
        query.bindValue(":date", mySchool.wDate.toString("yyyy-MM-dd"));
        if (!query.exec()) {
            qDebug() << "1st: " << query.lastError().text();
        } else {
            while (query.next()) {
                QSqlRecord rec = query.record();
                Grade oneGrade;
                oneGrade.id = query.value(rec.indexOf("id")).toInt();
                oneGrade.dig = query.value(rec.indexOf("dig")).toInt();
                oneGrade.let = query.value(rec.indexOf("let")).toString();
                allGrades.push_back(oneGrade);
            }
        }
        // Общее кол-во учеников
        for (int i = 0; i < allGrades.size(); i++) {
            strGet = "SELECT a.count FROM %1 AS a \
                      WHERE a.id = :id and a.date <= :date and a.date = (SELECT MAX(b.date) FROM %1 AS b \
                                                                       WHERE a.id = b.id  and b.date <= :date) \
                      ORDER BY a.id;";
            strGet = strGet.arg(getTryTableName("pupils"));
            query.prepare(strGet);
            query.bindValue(":id", allGrades[i].id);
            query.bindValue(":date", mySchool.wDate.toString("yyyy-MM-dd"));
            if (!query.exec()) {
                qDebug() << "2nd: " << query.lastError().text();
            } else {
                if (query.next()) {
                    QSqlRecord rec = query.record();
                    Grade oneGrade;
                    allGrades[i].pupil = query.value(rec.indexOf("count")).toInt();
                    mySchool.all += allGrades[i].pupil;
                }
            }
        }
        // Отсутствуют на дату
        for (int i = 0; i < allGrades.size(); i++) {
            strGet = "SELECT ill, other, comment FROM %1 WHERE id = :id and date = :date";
            strGet = strGet.arg(getTryTableName("truants"));
            query.prepare(strGet);
            query.bindValue(":id", allGrades[i].id);
            query.bindValue(":date", mySchool.wDate.toString("yyyy-MM-dd"));
            if (!query.exec()) {
                qDebug() <<  "3rd: " << query.lastError().text();
            } else {
                if (query.next()) {
                    QSqlRecord rec = query.record();
                    Grade oneGrade;
                    allGrades[i].ill = query.value(rec.indexOf("ill")).toInt();
                    allGrades[i].other = query.value(rec.indexOf("other")).toInt();
                    allGrades[i].comment = query.value(rec.indexOf("comment")).toString();
                    allGrades[i].absent = allGrades[i].ill + allGrades[i].other;
                    mySchool.ill += allGrades[i].ill;
                    mySchool.other += allGrades[i].other;
                    if (query.size() != 0) {
                        allGrades[i].done = true;
                    }
                }
            }
        }
    }
    {
        Grade oneGrade;
        gradesParallel.clear();
        oneGrade.dig = allGrades[0].dig;
        oneGrade.done = allGrades[0].done;
        for (int i = 0; i < allGrades.size(); ++i) {
            if (allGrades[i].dig == oneGrade.dig) {
                oneGrade.ill += allGrades[i].ill;
                oneGrade.other += allGrades[i].other;
                oneGrade.pupil += allGrades[i].pupil;
                oneGrade.absent += allGrades[i].absent;
                oneGrade.done = oneGrade.done && allGrades[i].done;
                if (i == allGrades.size() - 1) {
                    gradesParallel.push_back(oneGrade);
                }
            } else {
                gradesParallel.push_back(oneGrade);
                oneGrade.Clear();
                oneGrade = allGrades[i];
                if (i == allGrades.size() - 1) {
                    gradesParallel.push_back(oneGrade);
                }
            }
        }
    }
    flagBusy = false;
}

QString Settings::utf2win(QString inStr)
{
    #ifdef Q_OS_WIN
        QString desc = inStr;
        QTextCodec *codec = QTextCodec::codecForName("windows-1251");
        QString res(codec->fromUnicode(desc));
        return res;
    #endif
    #ifdef Q_OS_LINUX
        return inStr;
    #endif
    #ifdef Q_OS_MAC
        return inStr;
    #endif
}

QString Settings::getComputerName()
{
    QHostInfo hostInfo;
    return hostInfo.localHostName();
}

bool Settings::addGrade(Grade grade, QDate dateStart, QDate dateEnd, QDate dateEnter)
{
    if (!recordExists(grade.dig, grade.let, dateStart, dateEnd)) {
     // добавляем
        QSqlQuery query(db);
        QString strInsInSchool = "INSERT INTO %1 (dig, let, date_start, date_end) VALUES (:dig, :let, :date_start, :date_end);";
        QString strGetID = "SELECT id FROM %1 WHERE dig = :dig and let = :let and date_start = :date_start and date_end = :date_end;";
        QString strInsInPupils = "INSERT INTO %1 (id, count, date) VALUES (:id, :count, :date);";
        strInsInSchool = strInsInSchool.arg(getTryTableName("school"));
        strInsInPupils = strInsInPupils.arg(getTryTableName("pupils"));
        strGetID = strGetID.arg(getTryTableName("school"));
        query.prepare(strInsInSchool);
        query.bindValue(":dig", grade.dig);
        query.bindValue(":let", grade.let);
        query.bindValue(":date_start", dateStart.toString("yyyy-MM-dd"));
        query.bindValue(":date_end", dateEnd.toString("yyyy-MM-dd"));
        if (!query.exec()) {
            qDebug() << query.lastError().text();
            return false;
        }
        query.clear();
        query.prepare(strGetID);
        query.bindValue(":dig", grade.dig);
        query.bindValue(":let", grade.let);
        query.bindValue(":date_start", dateStart.toString("yyyy-MM-dd"));
        query.bindValue(":date_end", dateEnd.toString("yyyy-MM-dd"));
        if (!query.exec()) {
            qDebug() << query.lastError().text();
            return false;
        } else {
            query.next();
            QString ID = query.value(0).toString();
            query.prepare(strInsInPupils);
            query.bindValue(":id", ID);
            query.bindValue(":count", grade.pupil);
            query.bindValue(":date", dateEnter.toString("yyyy-MM-dd"));
            if (!query.exec()) {
                qDebug() << query.lastError().text();
                return false;
            }
        }
        return true;
    }
    return false;
}

// Удаление класса по его ID
bool Settings::deleteGrade(int ID)
{
    QDate date_start(QDate::currentDate());
    QDate date_end(QDate::currentDate());
    QString strQuery = "SELECT date_start, date_end FROM %1 WHERE id = :id";
    strQuery = strQuery.arg(getTryTableName("school"));
    if (db.open()) {
        QSqlQuery query(db);
        query.prepare(strQuery);
        query.bindValue(":id", ID);
        if (!query.exec()) {
            qDebug() << query.lastError().text();
        } else {
            QSqlRecord rec = query.record();
            while (query.next()) {
                date_start = query.value(rec.indexOf("date_start")).toDate();
                date_end = query.value(rec.indexOf("date_end")).toDate();
                }
        }
    } else {
        qDebug() << "Base not open!";
        return false;
    }
    if ((date_start == QDate::currentDate()) && (date_end == QDate::currentDate())) {
        qDebug() << "date start-end not found";
        return false;
    }   
    strQuery = "UPDATE %1 SET date_start = :date_end, date_end = :date_start WHERE (id = :id)";
    strQuery = strQuery.arg(getTryTableName("school"));
    if (db.open()) {
        QSqlQuery query(db);
        query.clear();
        query.prepare(strQuery);
        query.bindValue(":id", ID);
        query.bindValue(":date_end", date_end.toString("yyyy-MM-dd"));
        query.bindValue(":date_start", date_start.toString("yyyy-MM-dd"));
        return query.exec();
    } else {
        qDebug() << "Base not open!";
        return false;
    }
}

// TODO: объединить с updateTruants !!!!!!!!!!!!!! объединено
bool Settings::addTruants(int id, QDate date, int ill, int other, QString comment)
{
    QDateTime currentTime = QDateTime::currentDateTime();
    QString compName = getComputerName();
    QSqlQuery query(db);
    QString strAdd = "INSERT INTO %1 (id, date, ill, other, comp, time, comment) \
                 VALUES (:id, :date, :ill, :other, :comp, :time, :comment)";
    strAdd = strAdd.arg(getTryTableName("truants"));
    query.prepare(strAdd);
    query.bindValue(":id", id);
    query.bindValue(":date", date.toString("yyyy-MM-dd"));
    query.bindValue(":ill", ill);
    query.bindValue(":other", other);
    query.bindValue(":comp", compName);
    query.bindValue(":time", currentTime.toString("yyyy-MM-dd hh:mm:ss.z"));
    query.bindValue(":comment", comment);
    if (!query.exec()) {
        qDebug() << query.lastError().text();
        return false;
    } else {
        return true;
    }
}

// Изменение общего количества учеников в классе
bool Settings::updatePupils(int id, QDate date, int countPupils)
{
    QString strQuery;
    QSqlQuery query(db);
    if (db.open()) {
        if (recordExists(id, date, "pupils")) {
            strQuery = "UPDATE %1 SET count = :count WHERE (id = :id) and (date = :date)";
        } else {                 // Записи за дату нет - добавляем
            strQuery = "INSERT INTO %1 (id, count, date) VALUES (:id, :count, :date)";
        }
        strQuery = strQuery.arg(getTryTableName("pupils"));
        query.clear();
        query.prepare(strQuery);
        query.bindValue(":id", id);
        query.bindValue(":count", countPupils);
        query.bindValue(":date", date.toString("yyyy-MM-dd"));
        if (!query.exec()) {
            qDebug() << query.lastError().text();
        } else {
            return true;
        }
    } else {
        qDebug() << "Base not open!";
    }
    return false;
}

bool Settings::updateTruants(int id, QDate date, int ill, int other, QString comment)
{
    QString compName = getComputerName();
    QDateTime currentTime = QDateTime::currentDateTime();
    QSqlQuery query(db);
    QString strQuery;
    if (recordExists(id, date, "truants")) {
        strQuery = "UPDATE %1 SET ill = :ill, other = :other, comp = :comp, time = :time, \
                     comment = :comment WHERE (id = :id) and (date = :date)";
    } else {                 // Записи за дату нет - добавляем
        strQuery = "INSERT INTO %1 (id, date, ill, other, comp, time, comment) \
                     VALUES (:id, :date, :ill, :other, :comp, :time, :comment)";
    }
    strQuery = strQuery.arg(getTryTableName("truants"));
    if (db.open()) {
        query.prepare(strQuery);
        query.bindValue(":ill", ill);
        query.bindValue(":other", other);
        query.bindValue(":comp", compName);
        query.bindValue(":time", currentTime.toString("yyyy-MM-dd hh:mm:ss.z"));
        query.bindValue(":comment", comment);
        query.bindValue(":id", id);
        query.bindValue(":date", date.toString("yyyy-MM-dd"));
        if (!query.exec()) {
            qDebug() << query.lastError().text();
        } else {
            return true;
        }
    } else {
        qDebug() << "Base not open!";
    }
    return false;
}

void Settings::getLetters(QString digit)
{
    codesLetters.utfCodes.clear();
    codesLetters.winCodes.clear();

    QString strGetLet = "SELECT let FROM %1 WHERE dig = :dig and date_start = :date_start and date_end = :date_end";
    strGetLet = strGetLet.arg(getTryTableName("school"));
    if (db.open()) {
        QSqlQuery query(db);
        query.prepare(strGetLet);
        query.bindValue(":dig", digit);
        query.bindValue(":date_start", dateStart.toString("yyyy-MM-dd"));
        query.bindValue(":date_end", dateEnd.toString("yyyy-MM-dd"));
        if (!query.exec()) {
            qDebug() << query.lastError().text();
        } else {
            QSqlRecord rec = query.record();
            while (query.next()) {
                QString tmpL = query.value(rec.indexOf("let")).toString();
                codesLetters.utfCodes.append(tmpL);
                codesLetters.winCodes.append(tmpL);
                }
        }
    } else {
        qDebug() << "Base not open!";
    }
}

QStringList Settings::getDigits()
{
    QStringList result;
    QString strGetDig = "SELECT DISTINCT dig FROM %1 WHERE date_start = :date_start and date_end = :date_end ORDER BY dig";
    strGetDig = strGetDig.arg(getTryTableName("school"));
    if (db.open()) {
        QSqlQuery query(db);
        query.prepare(strGetDig);
        query.bindValue(":date_start", dateStart.toString("yyyy-MM-dd"));
        query.bindValue(":date_end", dateEnd.toString("yyyy-MM-dd"));
        if (!query.exec()) {
            qDebug() << query.lastError().text();
        } else {
            QSqlRecord rec = query.record();
            while (query.next()) {
                 result.append(query.value(rec.indexOf("dig")).toString());
                }
        }
    } else {
        qDebug() << "Base not open!";
    }
    return result;
}

bool Settings::recordExists(int ID, QDate date, QString tableName)
{
    QString strQuery = "SELECT * FROM %1 WHERE (id = :id) and (date = :date)";
    strQuery = strQuery.arg(getTryTableName(tableName));
    QSqlQuery query1(db);
    query1.prepare(strQuery);
    query1.bindValue(":id", ID);
    query1.bindValue(":date", date.toString("yyyy-MM-dd"));
    if (!query1.exec()) {
        qDebug() << query1.lastError().text();
    } else {
        if (query1.size() != 0) { // Запись за дату есть
            return true;
        }
    }
    return false;
}

bool Settings::recordExists(int dig, QString let, QDate date_start, QDate date_end)
{
    QString strQuery = "SELECT * FROM %1 WHERE dig = :dig and let = :let and date_start = :date_start and date_end = :date_end";
    strQuery = strQuery.arg(getTryTableName("school"));
    QSqlQuery query1(db);
    query1.prepare(strQuery);
    query1.bindValue(":dig", dig);
    query1.bindValue(":let", let);
    query1.bindValue(":date_start", date_start.toString("yyyy-MM-dd"));
    query1.bindValue(":date_end", date_end.toString("yyyy-MM-dd"));
    if (!query1.exec()) {
        qDebug() << query1.lastError().text();
    } else {
        if (query1.size() != 0) { // Запись за дату есть
            return true;
        } else {
            return false;
        }
    }
    return false;
}

QString Settings::getTryTableName(QString tableName)
{
    if (Settings::typeBD == MSQL) {
        return "truant." + tableName;
    } else if (Settings::typeBD == PSQL) {
        return "truant.public." + tableName;
    }
    return tableName;
}

bool Settings::getPosWindow() const
{
    return posWindow;
}

void Settings::setPosWindow(bool value)
{
    posWindow = value;
}

bool Settings::getSizeWindow() const
{
    return sizeWindow;
}

void Settings::setSizeWindow(bool value)
{
    sizeWindow = value;
}

int Settings::getXPos() const
{
    return xPos;
}

void Settings::setXPos(int value)
{
    xPos = value;
}

int Settings::getYPos() const
{
    return yPos;
}

void Settings::setYPos(int value)
{
    yPos = value;
}

int Settings::getWSize() const
{
    return wSize;
}

void Settings::setWSize(int value)
{
    wSize = value;
}

int Settings::getHSize() const
{
    return hSize;
}

void Settings::setHSize(int value)
{
    hSize = value;
}

QString Settings::getPathOut() const
{
    return pathOut;
}

void Settings::setPathOut(const QString &value)
{
    QString strTmp = value;
    while ((strTmp.endsWith("/")) || (strTmp.endsWith("\\"))) {
        strTmp = strTmp.left(strTmp.size() - 1);
    }
    pathOut = strTmp;
}

bool Settings::getQuitNoAsk() const
{
    return quitNoAsk;
}

void Settings::setQuitNoAsk(bool value)
{
    quitNoAsk = value;
}

bool Settings::getFlagBusy() const
{
    return flagBusy;
}

void Settings::setFlagBusy(bool value)
{
    flagBusy = value;
}

int Settings::getTimeRefresh() const
{
    return timeRefresh;
}

void Settings::setTimeRefresh(int value)
{
    timeRefresh = value;
}

bool Settings::getShowFullGrid() const
{
    return showFullGrid;
}

void Settings::setShowFullGrid(bool value)
{
    showFullGrid = value;
}
