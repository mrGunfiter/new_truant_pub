#ifndef SETTINGS_H
#define SETTINGS_H

#include <QSettings>
#include <QtSql>
#include <QHostInfo>
#include "defines.h"

class Settings
{
public:
    Settings(QString fileName);
    bool getIniFileExists() const;
    void setIniFileExists(bool value);
    bool readConfig();
    bool writeConfig();  
    int checkBase();
    bool createBase();
    bool loadFile2Base(QDate dateStart, QDate dateEnd, QString fileToBase);
    Grade getGrade(QDate wDate, int gDig, QString gLet);
    // Получение данных из таблицы SCHOOL и TRUANTS по ВСЕЙ школе
    void getSchool(QDate wDate);
    QString utf2win(QString inStr);
    QString getComputerName();
    QString getTryTableName(QString tableName);
    bool addGrade(Grade grade, QDate dateStart, QDate dateEnd, QDate dateEnter);
    bool deleteGrade(int ID);
    bool addTruants(int id, QDate date, int ill, int other, QString comment); // Можно убрать, работает updateTruants
    bool updatePupils(int id, QDate date, int countPupils);
    bool updateTruants(int id, QDate date, int ill, int other, QString comment);

    Codes codesLetters;

    // Тип, хост и порт сервера БД
    static typeBase typeBD;
    static QString host;
    static int port;

    // пользователь для создания БД
    static QString superUser;
    static QString superPassword;

    // Последний редактируемый класс
    static int lastDigit;
    static QString lastLetter;

    // Дата начала и окончания текущего года
    QDate dateStart;
    QDate dateEnd;

    QString const baseName = "truant";
    // пользователь для работы с БД
    QString const user = "truant_user";
    QString const password = "truAntPaSSw0rd";
    // БД
    QSqlDatabase db;

    bool getPosWindow() const;
    void setPosWindow(bool value);

    bool getSizeWindow() const;
    void setSizeWindow(bool value);

    int getXPos() const;
    void setXPos(int value);

    int getYPos() const;
    void setYPos(int value);

    int getWSize() const;
    void setWSize(int value);

    int getHSize() const;
    void setHSize(int value);

    QString getPathOut() const;
    void setPathOut(const QString &value);

    bool getQuitNoAsk() const;
    void setQuitNoAsk(bool value);
    void getLetters(QString digit);
    QStringList getDigits();

    bool getFlagBusy() const;
    void setFlagBusy(bool value);

    int getTimeRefresh() const;
    void setTimeRefresh(int value);

    bool getShowFullGrid() const;
    void setShowFullGrid(bool value);

private:
    QString fileNameIni;
    bool iniFileExists;
    bool quitNoAsk;
    bool posWindow;
    bool sizeWindow;
    int xPos;
    int yPos;
    int wSize;
    int hSize;
    bool showFullGrid;
    QString pathOut;
    bool flagBusy;
    int timeRefresh;
    bool recordExists(int ID, QDate date, QString tableName);
    bool recordExists(int dig, QString let, QDate date_start, QDate date_end);
};

#endif // SETTINGS_H
