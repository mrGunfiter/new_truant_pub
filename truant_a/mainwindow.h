#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtWidgets>
#include <QDialog>
#include <QDebug>
#include <QCloseEvent>
#include "../defines.h"
#include "../options.h"
#include "../settings.h"
#include "editmain.h"
#include "edittruants.h"
#include "info.h"

#include "../3rdparty/qtxlsx/src/xlsx/xlsxdocument.h"
#include "../3rdparty/qtxlsx/src/xlsx/xlsxformat.h"
#include "../3rdparty/qtxlsx/src/xlsx/xlsxcellrange.h"
#include "../3rdparty/qtxlsx/src/xlsx/xlsxworksheet.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

protected:
    void showEvent(QShowEvent *ev);
    void closeEvent(QCloseEvent *event);
    bool eventFilter(QObject *target, QEvent *event);

private slots:
    void slotTimer1();
    void slotTimer2();
    void slotTimer3();
    void on_pbClose_clicked();
    void savePosSize();
    void on_tb_options_clicked();
    void slotEditRecordMain();
    void slotEditRecordTruants();
    void slotAddRecord();
    void slotRemoveRecord();
    void slotCustomMenuRequested(QPoint pos);
    void on_pushButton_2_clicked();
    void on_pbRefresh_clicked();
    void on_deDate_userDateChanged();
    void on_tbv_all_cellClicked(int row);
    void on_deDate_dateChanged(const QDate);
    void on_tbv_all_cellDoubleClicked(int row);
    void on_pushButton_clicked();
private:
    void fillMainTable(int curGrade);
    void checkActivation();
    void checkLic();
    QStringList checkLicExp();
    void showEventHelper();
    void fillTable();
    int step;
    QString infoRun;
    Ui::MainWindow *ui;
    QTimer *timer1;
    QTimer *timer2;
    QTimer *timer3;
};

#endif // MAINWINDOW_H
