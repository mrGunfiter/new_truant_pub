#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QMessageBox>
#include <qmessagebox.h>
#include <QProgressBar>
#include <QTimer>

extern bool isAdmin;
extern Settings settings;
extern Grade currentGrade;
extern School mySchool;

static bool flag = false;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle(FULL_PROGRAMM_NAME);
    this->setWindowIcon(QIcon(":/ico/truant_t.ico"));
}


MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    event->ignore();
    Settings::lastDigit = ui->numb->currentText().toInt();
    Settings::lastLetter = ui->let->currentText();
    if (settings.getQuitNoAsk()) {
        savePosSize();
        settings.writeConfig();
        event->accept();
    } else {
        QMessageBox msgBox;
        msgBox.setWindowTitle(FULL_PROGRAMM_NAME);
        msgBox.setText("Завершить работу?");
        msgBox.setIcon(QMessageBox::Question);
        msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
        msgBox.setDefaultButton(QMessageBox::Cancel);
        if  (msgBox.exec() == QMessageBox::Ok) {
            savePosSize();
            settings.writeConfig();
            event->accept();
        }
    }
}

void MainWindow::showEvent(QShowEvent *ev)
{
    QMainWindow::showEvent(ev);
    showEventHelper();
    ui->ill_absent->setValue(currentGrade.ill);
    ui->ather_absent->setValue(currentGrade.other);
    if (settings.getPosWindow()) {
        move(settings.getXPos(), settings.getYPos());
    }
    if (settings.getSizeWindow()) {
        resize(settings.getWSize(), settings.getHSize());
    }
}

void MainWindow::savePosSize()
{
    settings.setXPos(pos().x());
    settings.setYPos(pos().y());
    settings.setHSize(height());
    settings.setWSize(width());
}

// отрисовка формы
void MainWindow::showEventHelper()
{
    ui->progressBar->setVisible(false);
    if (currentGrade.done){
        ui->done->setStyleSheet("color: green");
        ui->done->setText("Данные записаны!");
    }else{
        ui->done->setStyleSheet("color: black");
        ui->done->setText("Данные не записаны!");
    }
    ui->dateEdit->setDate(QDate::currentDate());
    ui->numb->clear();
    ui->numb->addItems(settings.getDigits());
    ui->numb->setCurrentText(QString::number(Settings::lastDigit));
    ui->let->clear();
    settings.getLetters(ui->numb->currentText());
    ui->let->addItems(settings.codesLetters.winCodes);
    ui->let->setCurrentText(Settings::lastLetter);
    getDateFromBase();    
}

void MainWindow::getDateFromBase()
{
    flag = true;
    currentGrade = settings.getGrade(ui->dateEdit->date(), ui->numb->currentText().toInt(), ui->let->currentText());
    ui->all->setText(QString::number(currentGrade.pupil));
    ui->now->setText(QString::number(currentGrade.pupil - currentGrade.absent));
    ui->absent->setText(QString::number(currentGrade.absent));
    ui->ill_absent->setValue(currentGrade.ill);
    ui->ather_absent->setValue(currentGrade.other);
    if (currentGrade.done){
        ui->done->setStyleSheet("color: green");
        ui->done->setText("Данные записаны!");
    }else{
        ui->done->setStyleSheet("color: black");
        ui->done->setText("Данные не записаны!");
    }
    ui->notes->setText(currentGrade.comment.trimmed());
    flag = false;
}

void MainWindow::on_pushButton_clicked()
{
    Options w_opt;
    w_opt.setWindowTitle("Настройки");
    w_opt.exec();
}

void MainWindow::on_pushButton_2_clicked()
{
    QString strText = "Программа учета отсутствующих в школе по классам\n\n(с) 2018-2020 Черевко Егор"
                      "\nМОБУ СОШ Агалатовский ЦО\n\nС++, Qt " + QString(qVersion());
    QMessageBox::about(this, FULL_PROGRAMM_NAME, strText);
}

void MainWindow::on_numb_currentTextChanged()
{
    ui->let->clear();
    settings.getLetters(ui->numb->currentText());

    ui->let->addItems(settings.codesLetters.winCodes);

    getDateFromBase();
}


void MainWindow::on_let_currentTextChanged()
{
    getDateFromBase();
}

void MainWindow::on_dateEdit_dateChanged()
{
    getDateFromBase();
    if (ui->dateEdit->date() != QDate::currentDate()) {
        ui->send->setDisabled(true);
    } else {
        ui->send->setDisabled(false);
    }
}

void MainWindow::on_send_clicked()
{
    if (currentGrade.done){
        QString strGrade = ui->numb->currentText() + "\"" + ui->let->currentText() + "\"";
        if (QMessageBox::warning(this, FULL_PROGRAMM_NAME, "Данные по " + strGrade +
                                 "УЖЕ ВВЕДЕНЫ!!!\n\n"
                                 "Берете ОТВЕТСТВЕННОСТЬ за изменение данных НА СЕБЯ?",
                                 QMessageBox::Yes | QMessageBox::No) == QMessageBox::No)
        {
            getDateFromBase();
            return;
        }
    }
    if (!settings.updateTruants(currentGrade.id, QDate::currentDate(), ui->ill_absent->value(),
                                ui->ather_absent->value(), ui->notes->toPlainText().trimmed())) {
        QMessageBox::critical(nullptr, FULL_PROGRAMM_NAME, "Не удалось записать!");
    }
    ui->done->setVisible(false);
    ui->progressBar->setVisible(true);
    for (int i = 0; i <= 100; i++){
        ui->progressBar->setValue(i);
        QThread::msleep(5);
    }
    QThread::msleep(1000);
    ui->progressBar->setVisible(false);
    ui->done->setVisible(true);
    getDateFromBase();
}

void MainWindow::spin()
{
    if (!flag) {
        QString ather = ui->ather_absent->text();
        QString ill = ui->ill_absent->text();
        ui->ill_absent->setRange(0,currentGrade.pupil - QString(ather).toInt());
        ui->ather_absent->setRange(0,currentGrade.pupil - QString(ill).toInt());
        ui->now->setText(QString::number(currentGrade.pupil - QString(ather).toInt() - QString(ill).toInt()));
        QString now = ui->now->text();
        ui->absent->setText(QString::number(currentGrade.pupil - QString(now).toInt()));
    }
}

void MainWindow::on_ill_absent_valueChanged(int)
{
    spin();
}

void MainWindow::on_ather_absent_valueChanged(int)
{
    spin();
}

void MainWindow::on_dateEdit_dateChanged(const QDate)
{
    ui->dateEdit->setMaximumDate(QDate::currentDate());
}
