#include "info.h"
#include "ui_info.h"

extern QString executerName;

Info::Info(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Info)
{
    ui->setupUi(this);
}

Info::~Info()
{
    delete ui;
}

void Info::showEvent(QShowEvent *ev)
{
    QDialog::showEvent(ev);
    showEventHelper();
}

void Info::showEventHelper()
{
    ui->leKey->setText(this->whatsThis());
    this->setWhatsThis("");
}

void Info::on_buttonBox_accepted()
{
    executerName = ui->leCode->text();
}
