#include "load.h"
#include "settings.h"
#include "ui_load.h"
#include <QDebug>
#include <QFileDialog>
#include <QMessageBox>
#include <QProgressDialog>

extern int isOK;
extern bool isAdmin;
extern Settings settings;

Load::Load(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Load)
{
    ui->setupUi(this);
    this->setWindowTitle(FULL_PROGRAMM_NAME);
    ui->lb_program->setStyleSheet(QString("font-size: %1px").arg(20));
    ui->lb_version->setText(VERSION);
    ui->lb_version->setStyleSheet(QString("font-size: %1px").arg(20));
}

Load::~Load()
{
    delete ui;
}

void Load::showEvent(QShowEvent *ev)
{
    QMainWindow::showEvent(ev);
    showEventHelper();
}

// Отрисовка формы
void Load::showEventHelper()
{
    if (!isAdmin) {
        ui->lb_user->setDisabled(true);
        ui->le_user->setDisabled(true);
        ui->le_user->setPlaceholderText("");
        ui->lb_pasword->setDisabled(true);
        ui->le_pasword->setDisabled(true);
        ui->le_pasword->setPlaceholderText("");
        ui->le_file->setDisabled(true);
        ui->lb_file->setDisabled(true);
        ui->tb_file->setDisabled(true);
        ui->lb_program->setText(PROGRAMM_NAME + "_T");
    } else {
        ui->lb_program->setText(PROGRAMM_NAME + "_A");
    }
    ui->pb_create->setDefault(true);
    qDebug() << "isOK" << isOK;
    QString showStr;
    QString typeBase = "";
    if (Settings::typeBD == MSQL) {
        typeBase = "MySQL";
    }
    else if (Settings::typeBD == PSQL) {
        typeBase = "PostgreSQL";
    }
    switch (isOK) {
        case 0: // нет ini-файла
            QMessageBox::critical(nullptr, FULL_PROGRAMM_NAME, "Отсутствует файл с настройками для подключения!\nВведите настройки и попробуйте снова!");
            break;
        case 1: // есть только ini-файл, настройки из него прочитать не удалось
            QMessageBox::critical(nullptr, FULL_PROGRAMM_NAME, "Не удалось прочитать файл с настройками для подключения!\nВведите настройки и попробуйте снова!");
            break;
        case 2: // Все OK, сюда попасть не должны
            break;
        case 3: // Прочитаны настройки из ини-файла, нет подключения к серверу SQL
            QMessageBox::critical(nullptr, FULL_PROGRAMM_NAME, "Не удалось подключиться к серверу SQL!\nПроверьте настройки и попробуйте снова!");
            ui->le_server->setText(settings.host + ":" + QString::number(settings.port));
            ui->comboBox->setCurrentText(typeBase);
            break;
        case 4: // Прочитаны настройки из ини-файла, есть подключение к серверу SQL, нет БД truant
            if (isAdmin) {
                showStr = "На сервере SQL не обнаружена рабочая БД!";
            } else {
                showStr = "На сервере SQL не обнаружена рабочая БД!\nОбратитесь к Администратору!";
            }
            QMessageBox::warning(nullptr, FULL_PROGRAMM_NAME, showStr);
            ui->le_server->setText(settings.host + ":" + QString::number(settings.port));
            ui->comboBox->setCurrentText(typeBase);
            break;
         case 5: // driver not loaded
            if (isAdmin) {
                showStr = "Отсутствует или не загружен драйвер " + typeBase;
            } else {
                showStr = "Отсутствует или не загружен драйвер " + typeBase + "!\nОбратитесь к Администратору!";
            }
            QMessageBox::critical(nullptr, FULL_PROGRAMM_NAME, showStr);
            ui->le_server->setText(settings.host + ":" + QString::number(settings.port));
            ui->comboBox->setCurrentText(typeBase);
//            ui->pb_create->setDisabled(true);
            break;
    }
}

// Пробуем подключиться к БД
void Load::on_pb_create_clicked()
{
   //TODO: проверка правильности данных !!!
   // ......
    if (ui->comboBox->currentText().toLower() == "mysql" )
        Settings::typeBD = MSQL;
    else if (ui->comboBox->currentText().toLower() == "postgresql" )
        Settings::typeBD = PSQL;
    QString deal = ui->le_server->text();
    QStringList deal_list = deal.split(':');
    QString numbers = "0123456789";
    int counter = 0;
    if (deal_list.size() == 2){
        QString deal_numb = deal_list[1];
        for (int i = 0; i < deal_list[1].size(); i++){
            for(int j = 0; j < 9; j++){
                if(numbers[j] == deal_numb[i]){
                    counter++;
                    break;
                }
            }
        }
        if(counter == deal_list[1].size()){
            Settings::host = deal_list[0];
            Settings::port = deal_list[1].toInt();
            settings.writeConfig();
        }else{
            QMessageBox::warning(nullptr, FULL_PROGRAMM_NAME, "Проверьте правильность введенных данных сервера и повторите авторизацию!");
            return;
        }
    }else{
        QMessageBox::warning(nullptr, FULL_PROGRAMM_NAME, "Проверьте правильность введенных данных сервера и повторите авторизацию!");
        return;
    }

    if (!isAdmin) {
        if (settings.checkBase() == 0) {
            QMessageBox::information(nullptr, FULL_PROGRAMM_NAME, "БД доступна\nПерезапустите программу!");
        } else {
            QMessageBox::critical(nullptr, FULL_PROGRAMM_NAME, ": БД НЕ доступна\nОбратитесь к Администратору!");
        }
    } else {
        if (isOK == 4) { // Сервер SQL доступен, пробуем создать БД
            QMessageBox msgBox;
            msgBox.setWindowTitle(FULL_PROGRAMM_NAME);
            msgBox.setText("Если это первый запуск программы, то необходимо создать БД");
            msgBox.setInformativeText("Cоздать БД?");
            msgBox.setIcon(QMessageBox::Question);
            msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
            msgBox.setDefaultButton(QMessageBox::Cancel);
            if  (msgBox.exec() == QMessageBox::Ok) {
                //TODO: Проверка всех условий
                // ................
                // Вызов ф-ции создания БД
                settings.superUser = ui->le_user->text();
                settings.superPassword = ui->le_pasword->text();
                if (settings.superUser.size() == 0 || settings.superPassword.size() == 0){
                    QMessageBox::warning(nullptr, FULL_PROGRAMM_NAME, "Проверьте правильность введенных данных пользователя и повторите авторизацию!");
                    return;

                }
                if ((ui->le_file->text().length() == 0)) {
                    if (QMessageBox::warning(nullptr, FULL_PROGRAMM_NAME, "При создании БД необходимо загрузить информацию \n"
                                            "на начало учебного года по всем классам школы!\n\n"
                                            "Файл для загрузки не указан, введите данные вручную после создания БД\n"
                                            "или выберите \"No\" и укажите файл для загрузки!\n\n"
                                            "Создать БД?",
                                            QMessageBox::Yes | QMessageBox::No) == QMessageBox::No) {
                        return;
                    }
                }
//                QProgressDialog progress("Создание Базы Данных...", "Отмена", 0, 0, this);
                QProgressDialog progress;
                progress.setLabelText("Создание Базы Данных...");
                progress.setWindowModality(Qt::WindowModal);
                progress.setMinimum(0);
                progress.setMaximum(0);
                progress.setValue(0);
                progress.show();
                if (settings.createBase()) {
                    progress.cancel();
                    QMessageBox::information(nullptr, FULL_PROGRAMM_NAME, "БД успешно создана!");
                    QFile file(ui->le_file->text());
                    if ( file.open(QFile::ReadOnly | QFile::Text) ) {
                    // TODO: загрузка файла в БД
                        QDate dateBegin;
                        QDate dateEnd;
                        if (QDate::currentDate().month() > 6) {
                            dateBegin.setDate(QDate::currentDate().year(), 9, 1);
                            dateEnd.setDate(QDate::currentDate().year() + 1, 5, 31);
                        } else {
                            dateBegin.setDate(QDate::currentDate().year() - 1, 9, 1);
                            dateEnd.setDate(QDate::currentDate().year(), 5, 31);
                        }
                        progress.setLabelText("Загрузка данных...");
                        progress.setWindowModality(Qt::WindowModal);
                        progress.setMaximum(0);
                        progress.setMinimum(0);
                        progress.setValue(0);
                        progress.show();
                        if (settings.loadFile2Base(dateBegin, dateEnd, ui->le_file->text())) {
                            progress.cancel();
                            QMessageBox::information(nullptr, FULL_PROGRAMM_NAME, "Данные загружены успешно!");
                        } else {
                            progress.cancel();
                            QMessageBox::critical(nullptr, FULL_PROGRAMM_NAME, "Данные загрузить не удалось!\nИли они загружены с ошибками!\n\nВозможно повреждение данных!");
                        }
                    }
                } else {
                    progress.cancel();
                    QMessageBox::critical(nullptr, FULL_PROGRAMM_NAME, "НЕ удалось создать БД!");
                }
            } else {
                return;
            }
        } else {
            QMessageBox::critical(nullptr, FULL_PROGRAMM_NAME, "НЕ удалось подключиться к SQL-серверу!");
//            return;
        }
    }
    // Проверка подключения
    int baseError = settings.checkBase();
    switch (baseError) {
        case 0:
            QMessageBox::information(nullptr, FULL_PROGRAMM_NAME, "БД доступна\nПерезапустите программу!");
            settings.writeConfig();
            this->close();
            break;
        case 1:
            QMessageBox::critical(nullptr, FULL_PROGRAMM_NAME, "SQL сервер не доступен!");
            break;
        case 2:
            QMessageBox::critical(nullptr, FULL_PROGRAMM_NAME, "БД НЕ доступна");
            break;
        case 3:
            QMessageBox::critical(nullptr, FULL_PROGRAMM_NAME, "Driver not loaded!");
    }
}

// TODO: Выбор файла для загрузки в БД
void Load::on_tb_file_clicked()
{
    ui->le_file->setText(QFileDialog::getOpenFileName(this,
                                                    QString::fromUtf8("Открыть файл"),
                                                    QDir::currentPath(),
                                                    "Файл для загрузки (*.csv);;All files (*.*)"));
}

