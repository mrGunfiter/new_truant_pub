#ifndef OPTIONS_H
#define OPTIONS_H

#include <QDialog>
#include <QMainWindow>
#include <QtWidgets>
#include "../defines.h"

namespace Ui {
class Options;
}

class Options : public QDialog
{
    Q_OBJECT

public:
    explicit Options(QWidget *parent = nullptr);
    ~Options();

protected:
    void showEvent(QShowEvent *ev);

private slots:
    void on_buttonBox_accepted();
    void on_tbFile2Base_clicked();
    void on_pbLoad_clicked();

    void on_tbPathOut_clicked();

    void on_buttonBox_clicked(QAbstractButton *button);

private:
    void showEventHelper();
    Ui::Options *ui;
};

#endif // OPTIONS_H
