#include "mainwindow.h"
#include "../load.h"
#include "../settings.h"
#include <QApplication>
#include <QMessageBox>

extern int isOK;
extern bool isAdmin;
extern Settings settings;
extern QString executerName;

void loadSplash(QSplashScreen *splash, QPainter *painter, QPixmap pixmap, QString message, int progress)
{
    splash->showMessage(message, Qt::AlignBottom | Qt::AlignCenter);
    painter->begin(&pixmap);
    painter->fillRect(
            PROGRESS_X_PX,
            PROGRESS_Y_PX,
            static_cast<int>(progress /100.0 * PROGRESS_WIDTH_PX),
            PROGRESS_HEIGHT_PX, Qt::gray
    );
    painter->end();
    splash->setPixmap(pixmap);
    QEventLoop loop;
    QTimer::singleShot(150, &loop, &QEventLoop::quit);
    loop.exec();
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QPixmap pixmap(":/img/truant.png");
    QSplashScreen splash(pixmap);
    splash.show();
    a.processEvents();
    QPainter painter;

    loadSplash(&splash, &painter, pixmap, "Загрузка...", 10);

    isAdmin = true;
    isOK = 0;

    QCoreApplication::setApplicationName(PROGRAMM_NAME);
    if (settings.getIniFileExists()) {
        isOK++;
        loadSplash(&splash, &painter, pixmap, "INI-файл найден...", 25);
        a.processEvents();
        if (settings.readConfig()) {
            isOK++;
            loadSplash(&splash, &painter, pixmap, "Настройки загружены...", 35);
            a.processEvents();
            if (Settings::typeBD == MSQL) {
                settings.db = QSqlDatabase::addDatabase("QMYSQL");
            } else if (Settings::typeBD == PSQL) {
                settings.db = QSqlDatabase::addDatabase("QPSQL");
            } else {
                QString typeBase = "";
                if (Settings::typeBD == MSQL) {
                    typeBase = "MySQL";
                }
                else if (Settings::typeBD == PSQL) {
                    typeBase = "PostgreSQL";
                }
                QMessageBox::critical(nullptr, FULL_PROGRAMM_NAME, "SQLDriver " + typeBase + " не поддерживается!\nЗавершение работы...");
                exit(1);
            }
            loadSplash(&splash, &painter, pixmap, "Драйвер БД загружен...", 50);
            a.processEvents();
            isOK += settings.checkBase();
            if (isOK == 2) {
                loadSplash(&splash, &painter, pixmap, "БД подключена...", 85);
                a.processEvents();
            }
        } else {
            QMessageBox::critical(nullptr, FULL_PROGRAMM_NAME, "Не кооректный INI-файл!\nЗавершение работы...");
            exit(1);
        }
    }
    MainWindow w_main;
    Load w_load;
    if (isOK == 2) {
        loadSplash(&splash, &painter, pixmap, "Запуск программы...", 100);
        a.processEvents();
        w_main.show();
        splash.finish(&w_main);
    } else {
        loadSplash(&splash, &painter, pixmap, "Запуск не удался...", 25);
        a.processEvents();
        w_load.show();
        splash.finish(&w_load);
    }

    return a.exec();
}
