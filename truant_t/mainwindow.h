#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtWidgets>
#include <QDialog>
#include <QDebug>
#include <QCloseEvent>
#include "../defines.h"
#include "../settings.h"
#include "../options.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

protected:
    void showEvent(QShowEvent *ev);
    void closeEvent(QCloseEvent *event);

private slots:
    void savePosSize();
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();
    void on_numb_currentTextChanged();
    void on_let_currentTextChanged();
    void on_dateEdit_dateChanged();
    void on_send_clicked();
    void on_ill_absent_valueChanged(int arg1);
    void on_ather_absent_valueChanged(int arg1);

    void on_dateEdit_dateChanged(const QDate);

private:
    void showEventHelper();
    void getDateFromBase();
    void spin();
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
