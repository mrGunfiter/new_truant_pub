#include <QDebug>
#include "editmain.h"
#include "ui_editmain.h"
#include "../defines.h"
#include "../settings.h"

extern Grade currentGrade;
extern School mySchool;
extern QVector <Grade> allGrades;
extern bool editGrade;
extern Settings settings;

EditMain::EditMain(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EditMain)
{
    ui->setupUi(this);
}

EditMain::~EditMain()
{
    delete ui;
}

void EditMain::showEvent(QShowEvent *ev)
{
    QDialog::showEvent(ev);
    showEventHelper();
}

void EditMain::showEventHelper()
{
    ui->deDateBegin->setDate(settings.dateStart);
    ui->deDateBegin->setDateRange(settings.dateStart, settings.dateStart);
    ui->deDateEnd->setDate(settings.dateEnd);
    ui->deDateEnd->setDateRange(settings.dateEnd, settings.dateEnd);
    ui->deChange->setDateRange(settings.dateStart, mySchool.wDate);
    if (editGrade) { // редактируем класс
        ui->numb->setCurrentText(QString::number(currentGrade.dig));
        ui->numb->setEnabled(false);
        ui->leLet->setText(currentGrade.let);
        ui->leLet->setReadOnly(true);
        ui->all->setValue(currentGrade.pupil);
        ui->deChange->setDate(mySchool.wDate);   
    } else { // добавляем новый класс
        settings.getLetters(ui->numb->currentText());
        ui->deChange->setDate(settings.dateStart);
        ui->deDateBegin->setDate(settings.dateStart);
        ui->deDateEnd->setDate(settings.dateEnd);
    }
}

void EditMain::on_buttonBox_accepted()
{
    if (ui->all->value() != 0) {
        if (editGrade) { // редактируем класс
           if (!settings.updatePupils(currentGrade.id, ui->deChange->date(), ui->all->value())) {
               QMessageBox::critical(nullptr, FULL_PROGRAMM_NAME, "Не удалось записать!");
           }
         } else { // добавляем новый класс
            Grade tmpGrade;
            tmpGrade.dig = ui->numb->currentText().toInt();
            tmpGrade.let = ui->leLet->text();
            tmpGrade.pupil = ui->all->value();
            if (!settings.addGrade(tmpGrade, ui->deDateBegin->date(), ui->deDateEnd->date(), ui->deChange->date())) {
                QMessageBox::critical(nullptr, FULL_PROGRAMM_NAME, "Не удалось добавить класс!\nВозможно такой класс уже существует.");
            }
        }
    } else {
        QMessageBox::information(nullptr, FULL_PROGRAMM_NAME, "Количество учеников не может быть равно 0!\nДанные НЕ сохранены!");
    }
}
