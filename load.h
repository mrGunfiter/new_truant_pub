#ifndef LOAD_H
#define LOAD_H

#include <QMainWindow>
#include "defines.h"

namespace Ui {
class Load;
}


class Load : public QMainWindow
{
    Q_OBJECT

public:
    explicit Load(QWidget *parent = nullptr);
    ~Load();
    QString exeName;
protected:
    void showEvent(QShowEvent *ev);
private slots:
    void on_pb_create_clicked();
    void on_tb_file_clicked();

private:
    void showEventHelper();
    Ui::Load *ui;
};

#endif // LOAD_H
