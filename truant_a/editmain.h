#ifndef EDITMAIN_H
#define EDITMAIN_H

#include <QDialog>
#include <QMainWindow>
#include <QtWidgets>

namespace Ui {
class EditMain;
}

class EditMain : public QDialog
{
    Q_OBJECT

public:
    explicit EditMain(QWidget *parent = nullptr);
    ~EditMain();

protected:
    void showEvent(QShowEvent *ev);

private slots:
    void on_buttonBox_accepted();

private:
    void showEventHelper();
    Ui::EditMain *ui;
};

#endif // EDITMAIN_H
